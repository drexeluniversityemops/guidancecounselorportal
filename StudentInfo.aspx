﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="StudentInfo.aspx.cs" Inherits="StudentInfo" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Student Info</title>
    <style type="text/css">
        .style3
        {
            FONT-WEIGHT: bold;
            FONT-SIZE: 12px;
            COLOR: black;
            FONT-FAMILY: verdana, helvetica, arial, sans-serif;
            width: 177px;
        }
    </style>
    <link href="styles/GuidanceConselor.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div id="Header" runat="server">
            <table style="width:100%;background-color:#666699;" >
                <tr>
                    <td>
                        <img src="images/header_new.jpg" alt="logo" />
                    </td>
                </tr>
            </table>
        </div>
        <div id="NavBar">
            <table style="width:100%;">
                <tr>
                    <td width="80%" style="padding:20px;">
                        <asp:Label ID="lblHello" runat="server" Text=""></asp:Label>
                    </td>
                    <td>
                       <asp:LinkButton runat="server" Text="<< Back To Main Page" ID="lbBack" PostBackUrl="~/main.aspx" CssClass="uportal-channel-text" />
                    </td>
                    <td>
                        <div align="center">
                            <a href="default.aspx" ><img height="26" alt="help"  src="images/help_dft.gif" width="39" border="0" name="help" />
                            <br />
							<span class="uportal-label">help</span>
							</a>
					    </div>
                    </td>
                </tr>
            </table>
        </div>
    <div>
        <table  cellpadding="4" cellspacing="0" width="75%" style="PADDING-LEFT:20px;BORDER-RIGHT: none; BORDER-TOP: none; BORDER-LEFT: none; BORDER-BOTTOM: none;" >
					<tr>
						<td><asp:label id="CompOrInCompLabel" Runat="server" Font-Bold="True" ForeColor="Black" CssClass="uportal-head14-bold"></asp:label></td>
					</tr>	
					<tr>
						<td><asp:label id="TextForCompInComp" Runat="server" Font-Bold="False" ForeColor="Black" CssClass="uportal-text-small"></asp:label></td>
					</tr>	
					<tr>
						<td><asp:label id="LabelLastUpload" Runat="server" Font-Bold="False" ForeColor="Black" CssClass="uportal-text-small"></asp:label></td>
					</tr>
				</table>
    </div>
    <P />
        <table style="PADDING-LEFT:20px;width:100%;">
            <tr>
                <td class="style3">Name:</td>
                <td><asp:Label ID="lblName" runat="server" CssClass="uportal-text-small"/></td>
            </tr>
            <tr>
                <td class="style3">Application Status:</td>
                <td><asp:Label ID="lblAppStatus" runat="server" CssClass="uportal-text-small"/></td>
            </tr>
            <tr>
                <td class="style3">Missing Items:</td>
                <td><asp:Label ID="lblMissingItems" runat="server" CssClass="uportal-text-small"/></td>
            </tr>
            <tr>
                <td class="style3">Decision:</td>
                <td><asp:Label ID="lblDecision" runat="server" CssClass="uportal-text-small"/></td>
            </tr>
        </table>
  </form>
</body>
</html>
