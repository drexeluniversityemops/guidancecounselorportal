﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;


public partial class EditIpAddress : System.Web.UI.Page
{
    //ih 12-20-12 made this
    HighSchool hs = new HighSchool();

    protected void Page_Load(object sender, EventArgs e)
    {
        User user = (User)Session["CurrUser"];        
    //    if (Session["Auth"] == null)
    //    {
    //        Response.Redirect("login.aspx");
    //    }
    //    else if (user.TypeOfAccess != "Administrator")
    //    {
    //        Response.Redirect("main.aspx");
    //    }
    //    else
    //    {
            if (!Page.IsPostBack)
            {
                lblWelcome.Text = "Welcome back, " + user.UserFirstName;
                LoadData();
            }
            else
            {
            }
       // }

    }
    private void LoadData()
    {
        ListItem li = new ListItem();
        li.Text = String.Empty;
        li.Value = String.Empty;
        li.Selected = true;


        DataTable dt = new DataTable();
        dt = hs.GetHighSchoolWithUsers();
        ddlSchool.DataSource = dt;
        ddlSchool.DataTextField = "HighSchoolName";
        ddlSchool.DataValueField = "HighSchoolCode";
        ddlSchool.DataBind();
        ddlSchool.Items.Insert(0,li);       

    }

    protected void ddlSchool_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        UpdatePanel.Visible = true;
        int highschoolid = Convert.ToInt32(ddlSchool.SelectedValue);
        DataSet ds = new DataSet();
        DataTable dt = new DataTable();
        DataRow dr = null;
        //dt = hs.GetHighSchoolWithUsers(highschoolid);
        ds = hs.GetHighSchoolUsers(highschoolid);
        dt = ds.Tables[0];
        GridView1.DataSource = dt;
        GridView1.DataBind();

        dt = new DataTable();
        dt = ds.Tables[1];
        dr = dt.Rows[0];
        txtIpAddress.Text = dr[0].ToString();
        hfIp.Value = dr[0].ToString();

        lblUpdated.Visible = false;
    }

    protected void btnPasswordReset_OnClick(object sender, EventArgs e)
    {
        Button button1 = (Button)sender;
        GridViewRow row = (GridViewRow)button1.Parent.Parent;
        HiddenField hiddenfield = (HiddenField)row.Cells[5].FindControl("hfID");
        User user = new User();
        user.UserID = hiddenfield.Value.ToString();
        user.ResetPassword(user);
        lblUpdated.Text = "Password has been changed and email sent";
        lblUpdated.Visible = true;
    }


    protected void btnUpdateIpAddress_OnClick(object sender, EventArgs e)
    {
        User user = new User();
        string oldip = hfIp.Value;
        string tempip = txtIpAddress.Text;
        string newip = tempip.Substring(0,tempip.IndexOf("."));
        tempip = tempip.Remove(0,(tempip.IndexOf(".")+1));
        newip = newip + "." + tempip.Substring(0, tempip.IndexOf("."));
        newip = newip + ".*.*";
        user.UpdateIpAddress(newip, Convert.ToInt32(ddlSchool.SelectedValue));
        txtIpAddress.Text = newip;
        lblUpdated.Text = "IpAddress has been updated";
        lblUpdated.Visible = true;

    }
}
