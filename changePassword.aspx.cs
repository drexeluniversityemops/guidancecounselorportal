﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class changePassword : System.Web.UI.Page
{
    requestAccessHandler handler = new requestAccessHandler();
    User user;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Auth"] == null)
        {
            Response.Redirect("login.aspx");
        }
        user = (User)Session["CurrUser"];
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        handler.changePassword(Convert.ToInt32(user.UserID), txtOldPassword.Text, txtNewPassword.Text);
        Response.Redirect("main.aspx");
    }
}
