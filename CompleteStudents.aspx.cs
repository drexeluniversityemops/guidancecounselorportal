﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class ApplicationList : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Auth"] == null)
        {
            Response.Redirect("login.aspx");
        }
        if (Request["id"] == "7777777")
        {
            rpStudentList.DataSource = Mock.completeStudents();
            rpStudentList.DataBind();
            CompOrInCompLabel.Text = "All Applications";
            TextForCompInComp.Text = "The following is a list of all students at your high school who have submitted application materials to Drexel.<br>To view or print an individual record, select the student’s name.";
            LabelLastUpload.Text = "Data Current As Of: " + DateTime.Today;
        }
        else
        {
            SQL data = new SQL();
            rpStudentList.DataSource = data.getStudentList(Request["id"], "C");
            rpStudentList.DataBind();
            CompOrInCompLabel.Text = "Complete Applications";
            TextForCompInComp.Text = "The following is a list of all students at your high school who have completed the application process for Drexel.<br>To view or print an individual record, select the student’s name.";
            LabelLastUpload.Text = "Data Current As Of: " + data.GetDataLastUpdate();
        }
    }
}
