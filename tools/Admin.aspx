﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Admin.aspx.cs" Inherits="Admin" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link href="../styles/GuidanceConselor.css" rel="stylesheet" type="text/css" />

    <title>Drexel Admissions Update</title>
    <style type="text/css">
        #form1
        {
            text-align: left;
        }
        .style1
        {
            width: 615px;
            text-align: center;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
        <div id="header" class="Header">
            <img src="../images/header_new.jpg" alt="Drexel Guidance Counselor Portal"/>
        </div>
        <div id="rule" class="Rule">&nbsp;
        </div>
        <div id="toolbar" class="Toolbar">
            <table>
                <tr>
                    <td><a href="../login.aspx">Login</a></td>
                    <td><img height="16" hspace="3" src="images/app_divider.gif" width="2" 
                            alt="Admin Tools"/></td>
                    <td><a href="../register.aspx">Register</a></td>
                    <td><img height="16" hspace="3" src="images/app_divider.gif" width="2" 
                            alt="Admin Tools"/></td>
                  <%--  <td><a href="Contact.aspx">Contact</a></td>--%>
                    <td>
                        <asp:HyperLink ID="email" runat="server" NavigateUrl="mailto:alk35@drexel.edu">Contact</asp:HyperLink>
                    </td>
                </tr>
            </table>
            
        </div>
    <br />
    <table align="center" width="250px">
        <tr>
            <td class="style1" colspan="2">
                
                <asp:Label ID="lblLoginError" runat="server" CssClass="ErrorMessage"></asp:Label>
            </td>
            
        </tr>
        <tr>
            <td class="style1">
                User Name:</td>
            <td>
                <asp:TextBox ID="txtEmail" runat="server"></asp:TextBox>
            </td>
            
        </tr>
        <tr>
            <td class="style1">
                Password:</td>
            <td>
                <asp:TextBox ID="txtPassword" runat="server" TextMode="Password"></asp:TextBox>
            </td>
           
        </tr>
        <tr>
            <td class="style1">
                &nbsp;</td>
            <td>
                <asp:Button ID="Button1" runat="server" Text="Login" onclick="Button1_Click" />
            </td>
           
        </tr>
    </table>
    </form>
    </body>
</html>
