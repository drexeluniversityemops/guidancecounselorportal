﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="register.aspx.cs" Inherits="register" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register src="linkBar.ascx" tagname="linkBar" tagprefix="uc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <link href="styles/GuidanceConselor.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" src="scripts/jscript.validation.js"></script>
    <script type="text/javascript" src="scripts/jscript.popup.js"></script>
    <title>Drexel Admissions Update</title>
    <style type="text/css">
        .style1
        {
            font-size: 10px;
            font-weight: bold;
            text-align: right;
            width: 149px;
        }
    </style>
</head>
<body onload="jscript.validation.requestAccessCheckBox();" style="height:100%;">
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
        <div id="header" class="Header">
            <img src="images/header_new.jpg" alt="Drexel Guidance Counselor Portal" />
        </div>
        <div id="rule" class="Rule">
            &nbsp;
        </div>
        <div id="toolbar" class="Toolbar">
            <table>
                <tr>
                    <td>
                        <uc1:linkBar ID="linkBar1" runat="server" />
                    </td>
                </tr>
            </table>
        </div>
        <div id="body" style="text-align: left;">
            <table>
                <tr>
                    <td colspan="2" class="HeaderText">
                        <br />
                        Register Your School for the GCP (Guidance Counselor Portal)
                    </td>
                </tr>
                <tr>
                    <td class="HeaderText2">
                        <asp:Label ID="lblHeaderDesc" runat="server" Text="Please give us information for the School and for A Registered Representative"
                            CssClass="HeaderText2" />
                                
                    </td>
                </tr>
            </table>
        </div>
        <div id="content">
            
                    <asp:Panel ID="pnForm" runat="server">
                        <table class="FormTable" cellpadding="5" >
                            <tr>
                                <td colspan="2" class="HeaderText3"><div align="left">School Information</div></td>
                            </tr>
                            <tr>
                                <td class="TitleText">
                                    CEEB Code:
                                </td>
                                <td class="RegularText">
                                    <asp:TextBox ID="txtCEEB" runat="server"></asp:TextBox>
                                    <asp:Label ID="lblCeebCodeVal" runat="server" CssClass="SmallErrorMessage" 
                                        Text="*Required" Visible="False"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" class="HeaderText3"><div align="left">Representative Information</div></td>
                            </tr>
                            <tr>
                                <td class="TitleText">
                                    First Name:
                                </td>
                                <td class="RegularText">
                                    <asp:TextBox ID="txtFirstName" runat="server"></asp:TextBox>
                                    <asp:Label ID="lblFirstNameVal" runat="server" CssClass="SmallErrorMessage" 
                                        Text="*Required" Visible="False"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="TitleText">
                                    Last Name:
                                </td>
                                <td class="RegularText">
                                    <asp:TextBox ID="txtLastName" runat="server"></asp:TextBox>
                                    <asp:Label ID="lblLastNameVal" runat="server" CssClass="SmallErrorMessage" 
                                        Text="*Required" Visible="False"></asp:Label>
                                </td>
                            </tr>
                            
                            <tr>
                                <td class="TitleText">
                                    Telephone:
                                </td>
                                <td class="RegularText">
                                    <asp:TextBox ID="txtPhone" runat="server"></asp:TextBox>
                                    <asp:Label ID="lblTelephoneVal" runat="server" CssClass="SmallErrorMessage" 
                                        Text="*Required" Visible="False"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="TitleText">
                                    Email:
                                </td>
                                <td class="RegularText">
                                    <asp:TextBox ID="txtEmail" runat="server"></asp:TextBox>
                                    <asp:Label ID="lblEmailVal" runat="server" CssClass="SmallErrorMessage" 
                                        Text="*Required" Visible="False"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="TitleText">
                                    Best Time to Contact:
                                </td>
                                <td class="RegularText">
                                    <asp:DropDownList ID="ddlContact" runat="server">
                                        <asp:ListItem>9 A.M. to 12 P.M. ET</asp:ListItem>
                                        <asp:ListItem>12 P.M. to 1 P.M. ET</asp:ListItem>
                                        <asp:ListItem>1 P.M. to 5 P.M. ET</asp:ListItem>
                                        <asp:ListItem>Any Time During ET Business Hours</asp:ListItem>
                                    </asp:DropDownList>
                                    <asp:Label ID="lblContactVal" runat="server" CssClass="SmallErrorMessage" 
                                        Text="*Required" Visible="False"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="TitleText">
                                </td>
                                <td class="RegularText">
                                    <asp:CheckBox ID="cbLimited" runat="server" Text="" />I understand that use of and
                                    access to this site is limited to Drexel University and registered high school guidance
                                    and college advisors.
                                </td>
                            </tr>
                            <tr>
                                <td class="style1">
                                </td>
                                <td class="RegularText">
                                    <asp:CheckBox ID="cbMisuse" runat="server" Text="" />I also understand that any
                                    misuse of this site including sharing of passwords, sharing of admission status
                                    reports with non-registered parties, and sharing of admission or applicant information
                                    will be cause for immediate loss of access for the high school.
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                    <asp:Panel ID="pnResults" runat="server" Visible="false">
                        <table class="FormTable" align="left" cellpadding="5">
                            <tr>
                                <td colspan="2" align="left">
                                    <asp:Label ID="lblResult" runat="server" CssClass="ErrorMessage" />
                                </td>
                            </tr>
                            <tr>
                                <td class="TitleText">
                                    Type of Access:
                                </td>
                                <td class="RegularText">
                                    <asp:Label ID="lblTypeofAccess" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="TitleText">
                                    First Name:
                                </td>
                                <td class="RegularText">
                                    <asp:Label ID="lblFirstName" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="TitleText">
                                    Last Name:
                                </td>
                                <td class="RegularText">
                                    <asp:Label ID="lblLastName" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="TitleText">
                                    CEEB:
                                </td>
                                <td class="RegularText">
                                    <asp:Label ID="lblCEEB" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="TitleText">
                                    Telephone:
                                </td>
                                <td class="RegularText">
                                    <asp:Label ID="lblPhone" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="TitleText">
                                    Email:
                                </td>
                                <td class="RegularText">
                                    <asp:Label ID="lblEmail" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="TitleText">
                                    Best Time to Contact:
                                </td>
                                <td class="RegularText">
                                    <asp:Label ID="lblContact" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <asp:LinkButton ID="lbEdit" runat="server" Text="Edit your submission" 
                                        Visible="false" onclick="lbEdit_Click"></asp:LinkButton>    
                                </td>
                                
                            </tr>
                        </table>
                    </asp:Panel>
                    <table align="left">
                        <tr>
                            <td class="TitleText">
                                &nbsp;
                            </td>
                            <td class="RegularText">
                                <asp:Button ID="btnSubmit" runat="server" OnClick="btnSubmit_Click" Text="Submit" />
                            </td>
                        </tr>
                    </table>
                </div>
               <%-- <cc1:ModalPopupExtender ID="LinkButton1_ModalPopupExtender" runat="server" 
                    TargetControlID="btnSubmit" 
                    PopupControlID="panel1" 
                    
                    BackgroundCssClass="modalBackground" 
                    DropShadow="false" >
               </cc1:ModalPopupExtender>--%>
               <asp:Panel runat="server" CssClass="modalBackground" ID="panel1" style="display:none;">
                    <div id="OKMessage" class="modalPopup">
                        <b></b>Submitting information...</b>
                    </div>
               </asp:Panel>
                
                
        </ContentTemplate>
    </asp:UpdatePanel>
  
    </form>
</body>
</html>
