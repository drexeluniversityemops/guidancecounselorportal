﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="changePassword.aspx.cs" Inherits="changePassword" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Change Password</title>
    <style type="text/css">
        .style1
        {
            width: 214px;
            text-align: right;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div id="Header" runat="server">
            <table style="width:100%;background-color:#666699;" >
                <tr>
                    <td>
                        <img src="images/header_new.jpg" alt="logo" />
                    </td>
                </tr>
            </table>
        </div>
        <div id="NavBar">
            <table style="width:100%;">
                <tr>
                    <td width="90%">
                    </td>
                    <td>
                        <div align="center">
                            <a href="default.aspx" ><img height="26" alt="help"  src="images/help_dft.gif" width="39" border="0" name="help" />
                            <br />
							<span class="uportal-label">help</span>
							</a>
					    </div>
                    </td>
                </tr>
            </table>
        </div>
        <div id="Body" runat="server" style="padding-left:20px;">
            <table style="width:100%;">
                <tr>
                    <td class="style1">
                        Old Password:
                    </td>
                    <td>
                        &nbsp;
                        <asp:TextBox ID="txtOldPassword" runat="server" TextMode="Password"></asp:TextBox>
                    </td>
                   
                </tr>
                <tr>
                    <td class="style1">
                        New Password:
                    </td>
                    <td>
                        &nbsp;
                        <asp:TextBox ID="txtNewPassword" runat="server" TextMode="Password"></asp:TextBox>
                    </td>
               </tr>
                <tr>
                    <td class="style1">
                     Confirm New Password:
                    </td>
                    <td>
                        &nbsp;
                        <asp:TextBox ID="TextBox3" runat="server" TextMode="Password"></asp:TextBox>
                    </td>
               </tr>
               <tr>
                <td>
                </td>
                <td>
                    <asp:Button ID="Button1" runat="server" Text="Submit" onclick="Button1_Click" />
                   </td>
               </tr>
            </table>
      </div>
    </form>
</body>
</html>
