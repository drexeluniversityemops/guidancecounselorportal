﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="CounselorAdmin.aspx.cs" Inherits="CounselorAdmin" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Admin Tools</title>
    <link href="styles/GuidanceConselor.css" rel="stylesheet" type="text/css" />
</head>
<body>
        <form id="form2" runat="server">
        <div id="header" class="Header">
            <img src="images/header_new.jpg" alt="Drexel Guidance Counselor Portal"/>
        </div>
        <div id="rule" class="Rule">&nbsp;
        </div>
        <div id="toolbar" class="Toolbar">
            <table width="100%";>
                <tr>
                    <td style="text-align:left;">
                        <asp:Label ID="lblWelcome" runat="server"></asp:Label>
                    </td>
                    <td style="text-align:right;">
                    <asp:LinkButton ID="lbBack" runat="server" PostBackUrl="~/main.aspx"><< Back to Main Page</asp:LinkButton>
                    </td>
                </tr>
            </table>
            
        </div>
        <a href="EditIpAddress.aspx">Edit School Ip Address and Reset Passwords</a>
        &nbsp
        <asp:HyperLink ID="HyperLink1" runat="server" Text="See User Data (all users)" NavigateUrl="~/ViewUserData.aspx" /> <%--//ds 10/3/14--%>
        <br />
    <asp:Label runat="server" ID="lblNumofRequests" Font-Bold="true" ForeColor="Red"/>
    <br />
    <asp:GridView ID="gvRequests" runat="server" AutoGenerateColumns="false"  OnRowCommand="handleRequest">
        <Columns>
            <asp:BoundField HeaderText="ID" DataField="userid" />
            <asp:BoundField HeaderText="First Name" DataField="userFirstName" />
            <asp:BoundField HeaderText="Last Name" DataField="userLastName" />
            <asp:BoundField HeaderText="Email" DataField="userEmail" />
            <asp:BoundField HeaderText="Phone" DataField="userPhone" />
            <asp:ButtonField CommandName="grant"  Text="Grant Access" ButtonType="Button"/>
            <asp:ButtonField CommandName="deny" Text="Deny Access" ButtonType="Button"/>
        </Columns>
    </asp:GridView>

    </form>
</body>
</html>
