﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class StudentInfo : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        User user = (User)Session["CurrUser"];
        DataTable studentInfo;
        SQL data = new SQL();
        int studentId = Convert.ToInt32(Request["id"]);
        int studentIndex = 0;
        if (user.Ceeb == "7777777")
        {
            studentInfo = Mock.allStudents();
            studentIndex = studentId-1;
        }
        else
        {
            
            studentInfo = data.getStudentInfo(Request["id"]);
        }
        lblName.Text = studentInfo.Rows[studentIndex]["Name"].ToString();

        lblAppStatus.Text = studentInfo.Rows[studentIndex]["Status"].ToString();
        if (studentInfo.Rows[studentIndex]["MissingItemCodeByBreaks"].ToString() != "")
        {
            lblMissingItems.Text = studentInfo.Rows[studentIndex]["MissingItemCodeByBreaks"].ToString();
        }
        else
        {
            lblMissingItems.Text = "None";
        }
        if (studentInfo.Rows[studentIndex]["Decision"].ToString() != "")
        {
            lblDecision.Text = studentInfo.Rows[studentIndex]["Decision"].ToString();
        }
        else
        {
            lblDecision.Text = "None";
        }
        
        CompOrInCompLabel.Text = "Student Information";
        TextForCompInComp.Text = "We have the following information for this Student:";
        LabelLastUpload.Text = "Data Current As Of: " + data.GetDataLastUpdate();
       
        //rpStudentList.DataSource = data.getStudentInfo(Request["id"]);
        //rpStudentList.DataBind();
    }
}
