using System;
using System.Collections;
using System.Data;


	public class AnnouncementList
	{
		#region Private Variables
		ArrayList _Announcements;
		#endregion

		#region Properties
		
		public ArrayList Announcements
		{
			get{return _Announcements;}
			set{_Announcements = value;}
		}
		#endregion

		#region Constructors
		public AnnouncementList(string ceeb)
		{
			getAnnouncements(ceeb);
		}
		#endregion

		#region Methods
		void getAnnouncements(string ceeb)
		{
			Announcements = new ArrayList();
			SQL data = new SQL();
			//ds.Tables["SchoolAdmin"].Rows[0]["userEmail"].ToString();
			foreach(DataRow dr in data.getAnnouncements(ceeb).Rows)
			{
				Announcement announcement = new Announcement((int)dr["announcementID"],dr["announcementhead"].ToString());
				Announcements.Add(announcement);
			}
		}
		#endregion
	}

