﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class DrexelAccess : System.Web.UI.Page
{
    requestAccessHandler handler = new requestAccessHandler();
    User user;

    protected void Page_Load(object sender, EventArgs e)
    {
        validateUser();
        lblWelcome.Text = "Welcome back, " + user.UserName;
        gvRequests.DataSource = handler.getRequests(user.UserEmail);
        gvRequests.DataBind();
        try
        {
            if (gvRequests.Rows[0] != null)
            {
                lblNumofRequests.Text = gvRequests.Rows.Count.ToString() + " new request(s)";
            }
        }
        catch
        {
            lblNumofRequests.Text = "No New Requests";
        }
        

    }
    private void validateUser()
    {
        //Check for session object
        if (Session["GC_User"] != null)
        {
            //make session object current user object
            user = (User)Session["GC_User"];
            //verify user is in database
            if (handler.checkValidatedUserAgainstDB(user) != true)
            {
                //redirect to admin login.
                Session["GC_User"] = null;
                Response.Redirect("admin.aspx?error=true");
            }
        }
        else
        {
            //there is no session object, redirect to login
            Response.Redirect("admin.aspx?error=true");
        }

    }
    public void handleRequest(Object src, GridViewCommandEventArgs e)
    {
        int index = Convert.ToInt32(e.CommandArgument);

        GridViewRow selectedRow = ((GridView)e.CommandSource).Rows[index];

        if (e.CommandName == "grant")
        {
            handler.grantAccess(Convert.ToInt16(selectedRow.Cells[0].Text), selectedRow.Cells[1].Text, selectedRow.Cells[2].Text, selectedRow.Cells[3].Text, generatePassword());
            Response.Redirect("drexelAccess.aspx");
           // Response.Write(selectedRow.Cells[1].Text);
            //Response.Write(index);

        }
        else if (e.CommandName == "deny")
        {
            handler.denyAccess(Convert.ToInt16(selectedRow.Cells[0].Text), selectedRow.Cells[1].Text, selectedRow.Cells[2].Text, selectedRow.Cells[3].Text);
            Response.Redirect("drexelAccess.aspx");
        }
    }
    public string generatePassword()
    {
        char[] alpha = { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z' };
        int[] numero = { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
        int whatNum = 0;

        string passwordRandom = "";
        Random r = new Random();

        whatNum = r.Next(1, 26);
        passwordRandom = passwordRandom + alpha[whatNum];
        whatNum = r.Next(1, 26);
        passwordRandom = passwordRandom + alpha[whatNum];
        whatNum = r.Next(1, 26);
        passwordRandom = passwordRandom + alpha[whatNum];
        whatNum = r.Next(1, 26);
        passwordRandom = passwordRandom + alpha[whatNum];
        whatNum = r.Next(1, 26);
        passwordRandom = passwordRandom + alpha[whatNum];
        whatNum = r.Next(1, 26);
        passwordRandom = passwordRandom + alpha[whatNum];
        whatNum = r.Next(1, 26);
        passwordRandom = passwordRandom + Convert.ToString(r.Next(1, 9));


        return passwordRandom;
    }

    protected void lbReports_Click(object sender, EventArgs e)
    {
        Response.Redirect("report.aspx");
    }
}