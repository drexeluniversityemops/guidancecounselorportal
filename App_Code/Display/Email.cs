﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.Mail;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;
using System.IO;

/// <summary>
/// Summary description for Email
/// </summary>
public class Email
{
    public string _FromEmailAddress;
    public string _ToEmailAddress;
    public string _CCEmailAddress;
	public Email(String FromAddress, String ToAddress, String ccAddress)
	{
        _FromEmailAddress = FromAddress;
        _ToEmailAddress = ToAddress;
     //   _CCEmailAddress = ccAddress;//ds 11/17/14
	}

    //ih 12/20/2012
    public Email() { }

    public void SendEmail(string body, String subject)
    {
        MailMessage email_message = new MailMessage();
        SmtpMail.SmtpServer = "smtp.mail.drexel.edu";
        email_message.To = _ToEmailAddress;
        email_message.From = _FromEmailAddress;
        email_message.Cc = _CCEmailAddress;
        email_message.Subject = subject;
        email_message.Body = body;
        email_message.BodyFormat = MailFormat.Html;
        try
        {
            SmtpMail.Send(email_message);
        }
        catch (Exception e)
        {

        }

    }
    //ih 2/20/2012
    public string ReplaceValues(List<string> variables, List<string> replacementvariables, string path_to_template)
    {
        string email_body = "";
        string emailstring = "";
        try
        {
            StreamReader sr = new StreamReader(path_to_template);
            emailstring = sr.ReadToEnd();
            for (int i = 0; i < variables.Count; i++)
            {
                emailstring = emailstring.Replace(replacementvariables[i],variables[i]);
            }
        }
        catch (Exception e)
        {
            email_body = "ERROR! " + e.ToString();
        }

        return emailstring;
    }

}
