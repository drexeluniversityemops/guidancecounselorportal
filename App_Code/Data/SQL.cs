﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

/// <summary>
/// Summary description for SQL
/// </summary>
public class SQL
{
    #region Constructors
    public SQL()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #endregion
    
    #region Methods
    public static int insertRequestor(int typeOfAccess, string IP, string firstName, string lastName, string email, string phone, int ceeb, string bestTimeToContact)
    {
        SqlConnection myConnection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["EM"].ConnectionString);
        SqlCommand myCommand = new SqlCommand("dbo.GC_InsertRequest", myConnection);
        myCommand.CommandType = CommandType.StoredProcedure;
        myCommand.Parameters.Add("@TypeOfAccess", typeOfAccess);
        myCommand.Parameters.Add("@IP", IP);
        myCommand.Parameters.Add("@FirstName", firstName);
        myCommand.Parameters.Add("@LastName", lastName);
        myCommand.Parameters.Add("@CEEB", ceeb);
        myCommand.Parameters.Add("@Email", email);
        myCommand.Parameters.Add("@Phone", phone);
        myCommand.Parameters.Add("@BestTimeToContact", bestTimeToContact);

        SqlParameter myParm = myCommand.Parameters.Add("@Return", SqlDbType.Int);
        myParm.Direction = ParameterDirection.Output;

        myConnection.Open();
        myCommand.ExecuteNonQuery();
        myConnection.Close();
        return ((int)myParm.Value);
    }
    public static int insertCounselor(int typeOfAccess, string IP, string firstName, string lastName, string email, string phone, string ceeb, string bestTimeToContact)
    {
        SqlConnection myConnection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["EM"].ConnectionString);
        SqlCommand myCommand = new SqlCommand("GC_InsertCounselorRequest", myConnection);
        myCommand.CommandType = CommandType.StoredProcedure;
        myCommand.Parameters.Add("@TypeOfAccess", typeOfAccess);
        myCommand.Parameters.Add("@IP", IP);
        myCommand.Parameters.Add("@FirstName", firstName);
        myCommand.Parameters.Add("@LastName", lastName);
        myCommand.Parameters.Add("@CEEB", ceeb);
        myCommand.Parameters.Add("@Email", email);
        myCommand.Parameters.Add("@Phone", phone);
        myCommand.Parameters.Add("@BestTimeToContact", bestTimeToContact);

        SqlParameter myParm = myCommand.Parameters.Add("@Return", SqlDbType.Int);
        myParm.Direction = ParameterDirection.Output;

        myConnection.Open();
        myCommand.ExecuteNonQuery();
        myConnection.Close();
        return ((int)myParm.Value);
    }
    public static User authencticateUser(String userName, String password)
    {
        User user = new User();
        SqlDataReader rdr = null;
        SqlConnection myConnection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["EM"].ConnectionString);
        SqlCommand myCommand = new SqlCommand("dbo.GC_authenticateUser", myConnection);
        myCommand.CommandType = CommandType.StoredProcedure;
        myCommand.Parameters.Add("@Email", userName);
        myCommand.Parameters.Add("@Password", password);
        myConnection.Open();
        rdr = myCommand.ExecuteReader();
        while (rdr.Read())
        {
            user = new User((String)rdr["UserID"], (String)rdr["UserEmail"], (String)rdr["UserPassword"], (String)rdr["UserFirstName"] + " " + (String)rdr["UserLastName"], (String)rdr["UserPhone"], (String)rdr["Ceeb"]);
        }

        myConnection.Close();
        return (user);

    }
    public DataTable getAnnouncements(string ceeb)
    {
        ArrayList parameters = new ArrayList();
        SqlParameter parmCeeb = new SqlParameter("@schoolid", ceeb);
        parameters.Add(parmCeeb);
        return getDataTable("sp_GC_getAnnouncementHeadToView", parameters, "Announcements");
    }
    public DataTable getAnnouncementBody(string id)
    {
        ArrayList parameters = new ArrayList();
        SqlParameter parmID = new SqlParameter("@announcementID", id);
        parameters.Add(parmID);
        return getDataTable("sp_GC_getAnnouncements", parameters, "Announcement");
    }
    public static List<String> getUniversalAnnouncements()
    {
        List<String> tempList = new List<String>();
        SqlDataReader rdr = null;
        SqlConnection con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["EM"].ConnectionString);
        SqlCommand command = new SqlCommand("dbo.GC_GetUniversalAnnouncements", con);
        command.CommandType = System.Data.CommandType.StoredProcedure;
        con.Open();
        rdr = command.ExecuteReader();
        while (rdr.Read())
        {
            tempList.Add((String)rdr["announcementText"]);
        }
        con.Close();
        return tempList;

    }
    
    public static DataTable getUserRequestReport()
    {
        DataTable tempTable = new DataTable();
        SqlConnection myConnection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["EM"].ConnectionString);
        SqlCommand myCommand = new SqlCommand("dbo.GC_getUserRequestReport", myConnection);
        myCommand.CommandType = CommandType.StoredProcedure;
        SqlDataAdapter adpt = new SqlDataAdapter();	//instantiate Data Adapter
        myConnection.Open(); //open connection
        adpt.SelectCommand = myCommand;//define command for adapter
        adpt.Fill(tempTable);	//fill adapter
        myConnection.Close();
        return tempTable;
    }
    public static String getCounselorEmailByCeeb(int ceeb)
    {
        SqlConnection myConnection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["EM"].ConnectionString);
        SqlCommand myCommand = new SqlCommand("dbo.GC_GetCounselorEmailByCeeb", myConnection);
        myCommand.CommandType = CommandType.StoredProcedure;
        myCommand.Parameters.Add("@Ceeb", ceeb);

        myConnection.Open();
        return (String)myCommand.ExecuteScalar();
        myConnection.Close();

    }
    private DataTable getDataTable(string storedProcedureName, ArrayList parameters, string tableName)
    {

        DataTable tempTable = new DataTable(tableName);
        SqlConnection myConnection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["EM"].ConnectionString);
        SqlCommand myCommand = new SqlCommand(storedProcedureName, myConnection);
        myCommand.CommandType = CommandType.StoredProcedure;
        foreach (SqlParameter parameter in parameters)
        {
            myCommand.Parameters.Add(parameter);
        }
        SqlDataAdapter adpt = new SqlDataAdapter();	//instantiate Data Adapter
        myConnection.Open(); //open connection
        adpt.SelectCommand = myCommand;//define command for adapter
        adpt.Fill(tempTable);	//fill adapter
        myConnection.Close();
        return tempTable;
    }
    public string GetDataLastUpdate()
    {
        string lastData = "";
        SqlConnection myConnection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["EM"].ConnectionString);
        SqlCommand myCommand = new
            SqlCommand("sp_GC_getLastUploadDate", myConnection);
        myCommand.CommandType = CommandType.StoredProcedure;
        try
        {
            myConnection.Open();
            SqlDataReader myReader = myCommand.ExecuteReader();
            while (myReader.Read())
            {
                lastData = myReader.GetString(0);
            }
        }
        catch
        {
            return "";
        }
        finally
        {
            myConnection.Close();
        }
        return lastData;

    }
    public static DataTable getDrexelContactInfo()
    {
        DataTable tempTable = new DataTable();
        SqlConnection myConnection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["EM"].ConnectionString);
        SqlCommand myCommand = new SqlCommand("dbo.GC_getDrexelAdminContactInfo", myConnection);
        myCommand.CommandType = CommandType.StoredProcedure;
        SqlDataAdapter adpt = new SqlDataAdapter();	//instantiate Data Adapter
        myConnection.Open(); //open connection
        adpt.SelectCommand = myCommand;//define command for adapter
        adpt.Fill(tempTable);	//fill adapter
        myConnection.Close();
        return tempTable;
    }
    public DataTable getEvents()
    {
        ArrayList parameters = new ArrayList();
        SqlParameter parmSelectValue = new SqlParameter("@selectvalue", "3");
        parameters.Add(parmSelectValue);
        SqlParameter parmRadSelect = new SqlParameter("@radselect", "  dc78.EventType.EventLevelID like ");
        parameters.Add(parmRadSelect);
        SqlParameter parmSortString = new SqlParameter("@sortstring", "EventTypeName, EventStart ASC");
        parameters.Add(parmSortString);
        SqlParameter parmSelectOther = new SqlParameter("@SelectOther", " and EventDisplay=1 and EventDisplayStart < '" + DateTime.Now + "' and EventDisplayEnd > '" + DateTime.Now + "'");
        parameters.Add(parmSelectOther);
        return getDataTable("dc78.sp_Event_selForGC", parameters, "Events");
    }
    public DataTable getEventType()
    {
        //create parameters
        ArrayList parameters = new ArrayList();
        SqlParameter parmSelectValue = new SqlParameter("@selectvalue", "3");
        parameters.Add(parmSelectValue);
        SqlParameter parmRadSelect = new SqlParameter("@radselect", " dc78.EventType.EventLevelID like ");
        parameters.Add(parmRadSelect);
        SqlParameter parmSortString = new SqlParameter("@sortstring", "EventTypeOrder");
        parameters.Add(parmSortString);
        SqlParameter parmSelectOther = new SqlParameter("@SelectOther", " and EventTypeDisplay = 1 ");
        parameters.Add(parmSelectOther);
        //return DataTable
        return getDataTable("dc78.sp_Event_EventType_selForGC", parameters, "EventType");
    }
    public static DataTable getNewUsers(String email)
    {
        
       // getNewUsersForDrexelAdmin
        DataTable tempTable = new DataTable();
        SqlConnection myConnection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["EM"].ConnectionString);
        SqlCommand myCommand = new SqlCommand("dbo.GC_getNewUsersForDrexelAdmin", myConnection);
		myCommand.CommandType = CommandType.StoredProcedure;
        myCommand.Parameters.Add("@Email", email);
		SqlDataAdapter adpt = new SqlDataAdapter();	//instantiate Data Adapter
		myConnection.Open(); //open connection
		adpt.SelectCommand = myCommand;//define command for adapter
		adpt.Fill(tempTable);	//fill adapter
		myConnection.Close();
		return tempTable;
    }
    public static DataTable getNewCounselorUsers(String ceeb)
    {
        // getNewUsersForDrexelAdmin
        DataTable tempTable = new DataTable();
        SqlConnection myConnection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["EM"].ConnectionString);
        SqlCommand myCommand = new SqlCommand("dbo.GC_getNewUsersForCounselorAdmin", myConnection);
        myCommand.CommandType = CommandType.StoredProcedure;
        myCommand.Parameters.Add("@CEEB", ceeb);
        SqlDataAdapter adpt = new SqlDataAdapter();	//instantiate Data Adapter
        myConnection.Open(); //open connection
        adpt.SelectCommand = myCommand;//define command for adapter
        adpt.Fill(tempTable);	//fill adapter
        myConnection.Close();
        return tempTable;
    }
    public DataTable getNoEvents()
    {
        ArrayList parameters = new ArrayList();
        SqlParameter parmSelectValue = new SqlParameter("@selectvalue", "'%'");
        parameters.Add(parmSelectValue);
        SqlParameter parmRadSelect = new SqlParameter("@radselect", "  dc78.EventType.EventLevelID like ");
        parameters.Add(parmRadSelect);
        SqlParameter parmSortString = new SqlParameter("@sortstring", "EventTypeOrder");
        parameters.Add(parmSortString);
        SqlParameter parmSelectOther = new SqlParameter("@SelectOther", "");
        parameters.Add(parmSelectOther);
        return getDataTable("dc78.sp_Event_EventType_selForGC", parameters, "NoEvent");
    }
    public DataTable getSchoolAdminInfo(string ceeb)
    {
        ArrayList parameters = new ArrayList();
        SqlParameter parmCeeb = new SqlParameter("@schoolid", ceeb);
        parameters.Add(parmCeeb);
        return getDataTable("dbo.sp_GCgetAdminForSchool", parameters, "SchoolAdmin");
    }
    public DataTable getSchoolInfo(string ceeb)
    {
        ArrayList parameters = new ArrayList();
        SqlParameter parmSchoolID = new SqlParameter("@schoolid", ceeb);
        parameters.Add(parmSchoolID);
        return getDataTable("sp_GCgetSchoolInformation", parameters, "SchoolInfo");
    }
    public DataTable getSchoolInfoByCeeb(string ceeb)
    {
        ArrayList parameters = new ArrayList();
        SqlParameter parmCeeb = new SqlParameter("@schoolID", ceeb);
        parameters.Add(parmCeeb);
        return getDataTable("GC_getSchoolAddressForEdit", parameters, "SchoolInfo");
    }
    public static String getSchoolNameByCeeb(int ceeb)
    {
        SqlConnection myConnection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["EM"].ConnectionString);
        SqlCommand myCommand = new SqlCommand("GC_GetSchoolNameByCeeb", myConnection);
        myCommand.CommandType = CommandType.StoredProcedure;
        myCommand.Parameters.Add("@Ceeb", ceeb);

        myConnection.Open();
        return (String)myCommand.ExecuteScalar();
        myConnection.Close();
    }
    public static int getStudentCount(string schoolcode, char compOrInComp)
    {
        int numberOfStudents = 0;
        SqlConnection myConnection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["EM"].ConnectionString);
        SqlCommand myCommand = new
            SqlCommand("sp_GCgetIncompleteCompleteStudentsCount", myConnection);
        myCommand.CommandType = CommandType.StoredProcedure;

        myCommand.Parameters.Add(new
            SqlParameter("@schoolcode", SqlDbType.Int)).Value = schoolcode;
        myCommand.Parameters.Add(new
            SqlParameter("@compOrInComp", SqlDbType.NVarChar)).Value = compOrInComp;
        try
        {
            myConnection.Open();
            SqlDataReader myReader = myCommand.ExecuteReader();
            while (myReader.Read())
            {
                numberOfStudents = myReader.GetInt32(0);
            }
        }
        catch
        {
            return 0;
        }
        finally
        {
            myConnection.Close();
        }
        return numberOfStudents;
    }
    public DataTable getStudentInfo(string studentID)
    {
        ArrayList parameters = new ArrayList();
        SqlParameter parmStudentId = new SqlParameter("@studentid", studentID);
        parameters.Add(parmStudentId);
        return getDataTable("GC_GetStudentInfoByID", parameters, "StudentInfo");
    }
    public DataTable getStudentList(string ceeb, string type)
    {
        string storedProcedure = "";
        ArrayList parameters = new ArrayList();
        SqlParameter parmCeeb = new SqlParameter("@schoolID", ceeb);
        parameters.Add(parmCeeb);

        switch (type)
        {
            case "C":
                storedProcedure = "GC_GetStudentListComplete";
                break;
            case "I":
                storedProcedure = "GC_GetStudentListIncomplete";
                break;
            case "A":
                storedProcedure = "GC_GetStudentListAll";
                break;
        }
        return getDataTable(storedProcedure, parameters, "StudentList");
    }
    public DataTable getTerritoryManager(string ceeb)
    {
        ArrayList parameters = new ArrayList();
        SqlParameter parmCeeb = new SqlParameter("@CeebCode", ceeb);
        parameters.Add(parmCeeb);
        return getDataTable("GC_GetCounselor", parameters, "TerritoryManager");
    }

    //ds 10/3/14 - new feature so admin's can "spoof" users
    public static DataTable GetUserData()
    {
        DataTable dt = new DataTable();
        SqlConnection myConnection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["EM"].ConnectionString);
        try
        {
            SqlCommand command = new SqlCommand("GC_GetUserData", myConnection);
            command.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            adapter.Fill(dt);
        }
        catch (Exception e)
        {
        }
        finally
        {
            myConnection.Close();
        }
        return dt;
    }

    //ds 10/3/14 - new feature to record when user last logged in
    public void UpdateUserLastLogin(int userID)
    {
        try
        {
            SqlConnection myConnection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["EM"].ConnectionString);
            SqlCommand myCommand = new SqlCommand("dbo.GC_UpdateUserLastLogin", myConnection);
            myCommand.CommandType = CommandType.StoredProcedure;
            myCommand.Parameters.Add("@userID", userID);
            myConnection.Open();
            myCommand.ExecuteNonQuery();
            myConnection.Close();
        }
        catch { }
    }

    //ds 9/29/14 - new feature so admin's can "spoof" users
    public DataTable GetUserDataAdmin(string email)
    {
        ArrayList parameters = new ArrayList();
        SqlParameter parmEmail = new SqlParameter("@email", email);
        parameters.Add(parmEmail);
        return getDataTable("GC_getLoginInfo_admin", parameters, "User");
    }
    public DataTable GetUserData(string email, string password, string ip)
    {
        ArrayList parameters = new ArrayList();
        SqlParameter parmEmail = new SqlParameter("@email", email);
        SqlParameter parmIp = new SqlParameter("@ip", ip);
        SqlParameter parmPassword = new SqlParameter("@password", password);

        parameters.Add(parmEmail);
        parameters.Add(parmIp);
        parameters.Add(parmPassword);
        return getDataTable("GC_getLoginInfo", parameters, "User");
    }
    public DataTable getUserforAdmin(string userID)
    {
        ArrayList parameters = new ArrayList();
        SqlParameter parmUserID = new SqlParameter("@schoolID", userID);
        parameters.Add(parmUserID);
        return getDataTable("GC_GetUsersForAdmin", parameters, "UserInfo");
    }
    public static void grantAccessToUser(int userID, String password)
    {
        SqlConnection myConnection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["EM"].ConnectionString);
        SqlCommand myCommand = new SqlCommand("dbo.GC_GrantUser", myConnection);
        myCommand.CommandType = CommandType.StoredProcedure;
        myCommand.Parameters.Add("@ID", userID);
        myCommand.Parameters.Add("@Password", password);
        myConnection.Open();
        myCommand.ExecuteNonQuery();
        myConnection.Close();
    }
    public static void grantAccessToCounsleor(int userID, String password)
    {
        SqlConnection myConnection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["EM"].ConnectionString);
        SqlCommand myCommand = new SqlCommand("dbo.GC_GrantCounselor", myConnection);
        myCommand.CommandType = CommandType.StoredProcedure;
        myCommand.Parameters.Add("@ID", userID);
        myCommand.Parameters.Add("@Password", password);
        myConnection.Open();
        myCommand.ExecuteNonQuery();
        myConnection.Close();
    }
    public static void denyAccessToUser(int userID)
    {
        SqlConnection myConnection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["EM"].ConnectionString);
        SqlCommand myCommand = new SqlCommand("dbo.GC_DenyUser", myConnection);
        myCommand.CommandType = CommandType.StoredProcedure;
        myCommand.Parameters.Add("@ID", userID);

        myConnection.Open();
        myCommand.ExecuteNonQuery();
        myConnection.Close();
    }
    public static void changePassword(int userID,String oldPassword, String newPassword)
    {
        SqlConnection myConnection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["EM"].ConnectionString);
        SqlCommand myCommand = new SqlCommand("dbo.GC_ChangeUserPassword", myConnection);
        myCommand.CommandType = CommandType.StoredProcedure;
        myCommand.Parameters.Add("@NewPassword", newPassword);
        myCommand.Parameters.Add("@OldPassword", oldPassword);
        myCommand.Parameters.Add("@UserID", userID);
        myConnection.Open();
        myCommand.ExecuteNonQuery();
        myConnection.Close();
        return;
    }
    public void UpdateSchoolInfo(string ceeb, string schoolname, string address, string city, string state, string zip, string userID)
    {
        SqlConnection myConnection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["EM"].ConnectionString);
        SqlCommand myCommand = new
            SqlCommand("GC_UpdateSchoolInfo", myConnection);
        myCommand.CommandType = CommandType.StoredProcedure;
        myCommand.Parameters.Add("@schoolid", ceeb);
        myCommand.Parameters.Add("@schoolname", schoolname);
        myCommand.Parameters.Add("@Address1", address);
        myCommand.Parameters.Add("@city", city);
        myCommand.Parameters.Add("@state", state);
        myCommand.Parameters.Add("@zip", zip);
        myCommand.Parameters.Add("@whoUpdated", userID);
        myConnection.Open();
        myCommand.ExecuteNonQuery();
        myConnection.Close();
    }
    public void updateUserInfo(string userID, string userFirstName, string userLastName, string userEmail, string userRequest)
    {
        SqlConnection myConnection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["EM"].ConnectionString);
        SqlCommand myCommand = new
            SqlCommand("GC_UpdateUserInfo", myConnection);
        int request;
        if (userRequest == "True")
        {
            request = 1;
        }
        else
        {
            request = 0;
        }
        myCommand.CommandType = CommandType.StoredProcedure;
        myCommand.Parameters.Add("@UserID", userID);
        myCommand.Parameters.Add("@FirstName", userFirstName);
        myCommand.Parameters.Add("@LastName", userLastName);
        myCommand.Parameters.Add("@Email", userEmail);
        myCommand.Parameters.Add("@Request", request);
        myConnection.Open();
        myCommand.ExecuteNonQuery();
        myConnection.Close();
    }
    public void UpdatePassword(string userID, string password)
    {
        SqlConnection myConnection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["EM"].ConnectionString);
        SqlCommand myCommand = new
            SqlCommand("GC_UpdateUserEmailPassword", myConnection);
        myCommand.CommandType = CommandType.StoredProcedure;
        myCommand.Parameters.Add("@UserID", userID);
        myCommand.Parameters.Add("@Password", password);
        myConnection.Open();
        myCommand.ExecuteNonQuery();
        myConnection.Close();
    }
    public void updatePasswordStatus(string userID)
    {
        SqlConnection myConnection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["EM"].ConnectionString);
        SqlCommand myCommand = new
            SqlCommand("GC_UpdateUserPasswordStatus", myConnection);
        myCommand.CommandType = CommandType.StoredProcedure;
        myCommand.Parameters.Add("@UserID", userID);
        myConnection.Open();
        myCommand.ExecuteNonQuery();
        myConnection.Close();
    }

    //ih 12-20-12 
    public DataTable GetHighSchoolsWithUsers()
    {
        DataTable dt = new DataTable();
        SqlConnection myConnection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["EM"].ConnectionString);
        try
        {
            SqlCommand command = new SqlCommand("GC_GetHighSchoolsWithUsers", myConnection);
            command.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            adapter.Fill(dt);
        }
        catch (Exception e)
        {
        }
        finally
        {
            myConnection.Close();
        }
        return dt;
    }

    //ih 12/20/12
    public DataSet GetHighSchoolUsers(int highschoolid)
    {
        DataSet ds = new DataSet();
        SqlConnection myConnection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["EM"].ConnectionString);
        try
        {
            SqlCommand command = new SqlCommand("GC_GetHighSchoolUsers", myConnection);
            command.CommandType = CommandType.StoredProcedure;
            SqlParameter param1 = new SqlParameter("@HighSchoolCode",SqlDbType.Int);
            param1.Value = highschoolid;
            command.Parameters.Add(param1);
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            adapter.Fill(ds);
        }
        catch (Exception e)
        {
        }
        finally
        {
            myConnection.Close();
        }
        return ds;
    }
    //ih 12/20/2012
    public User GetUserInfoById(User user)
    {
        SqlConnection myConnection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["EM"].ConnectionString);
        try
        {
            myConnection.Open();
            SqlCommand command = new SqlCommand("GC_GetUserInformationByID", myConnection);
            command.CommandType = CommandType.StoredProcedure;
            SqlParameter param1 = new SqlParameter("UserID", SqlDbType.Int);
            param1.Value = Convert.ToInt32(user.UserID);
            command.Parameters.Add(param1);
            SqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                user.UserEmail = reader["userEmail"].ToString();
                user.UserFirstName = reader["userFirstName"].ToString();
                user.UserLastName = reader["userLastName"].ToString();
                user.UserPassword = reader["userPassword"].ToString();
                user.UserPhone = reader["userPhone"].ToString();
                user.TypeOfAccess = reader["TypeOfAccess"].ToString();
            }
        }
        catch (Exception e)
        {
        }
        finally
        {
            myConnection.Close();
        }
        return user;
    }

    //ih 12/20/2012
    public void UpdateIpAddress(string ipaddress, int highschoolid)
    {
        
        SqlConnection myConnection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["EM"].ConnectionString);
        try
        {
            myConnection.Open();
            SqlCommand command = new SqlCommand("GC_UpdateIPAddressesByHighSchoolID", myConnection);
            command.CommandType = CommandType.StoredProcedure;
            SqlParameter param = new SqlParameter("@ipaddress", SqlDbType.VarChar);
            param.Value = ipaddress;
            command.Parameters.Add(param);
            param = new SqlParameter("@highschoolid", SqlDbType.Int);
            param.Value = highschoolid;
            command.Parameters.Add(param);
            command.ExecuteNonQuery();
        }
        catch (Exception e)
        {
        }
        finally
        {
            myConnection.Close();
        }
    }

    #endregion
}
