﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

/// <summary>
/// Summary description for DrexelAdmin
/// </summary>
public class DrexelAdminInfo
{
    String _Name;
    String _Phone;
    String _Email;
    String _DrexelAdminLink;
    String _CounselorAdminLink;
    String _SiteLoginLink;

    public String Name
    {
        get { return _Name; }
        set { _Name = value; }
    }
    public String Phone
    {
        get { return _Phone; }
        set { _Phone = value; }
    }
    public String Email
    {
        get { return _Email; }
        set { _Email = value; }
    }
    public String DrexelAdminLink
    {
        get { return _DrexelAdminLink; }
        set { _DrexelAdminLink = value; }
    }
    public String CounselorAdminLink
    {
        get { return _CounselorAdminLink; }
        set {_CounselorAdminLink = value;}
    }
    public String SiteLoginLink
    {
        get { return _SiteLoginLink; }
        set { _SiteLoginLink = value; }
    }
	public DrexelAdminInfo()
	{
		//DataTable tempTable = SQL.
	}
}
