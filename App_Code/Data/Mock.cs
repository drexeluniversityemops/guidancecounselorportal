﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

/// <summary>
/// Summary description for Mock
/// </summary>
public class Mock
{

    public static DataTable completeStudents()
    {
        DataTable dt = new DataTable(); ;
        DataRow student1 = dt.NewRow();
        DataRow student2 = dt.NewRow();
        DataRow student3 = dt.NewRow();
        DataRow student4 = dt.NewRow();
        
        dt.Columns.Add("studentid");
        dt.Columns.Add("Name");
        dt.Columns.Add("Decision");
        dt.Columns.Add("Status");

        //student 1
        student1["studentid"] = "1";
        student1["Name"] = "Susan Adams";
        student1["Decision"] = "Accept";
        student1["Status"] = "Complete";

        //student 2
        student2["studentid"] = "2";
        student2["Name"] = "John Smith";
        student2["Decision"] = "Withdrawn By Student Request";
        student2["Status"] = "Complete";

        //student 3
        student3["studentid"] = "3";
        student3["Name"] = "Mike Jordan";
        student3["Decision"] = "Denied";
        student3["Status"] = "Complete";

        //student 4
        student4["studentid"] = "4";
        student4["Name"] = "Barack Obama";
        student4["Decision"] = "Confirmed";
        student4["Status"] = "Complete";

        dt.Rows.Add(student1);
        dt.Rows.Add(student2);
        dt.Rows.Add(student3);
        dt.Rows.Add(student4);

        return dt;

    }
    public static DataTable incompleteStudents()
    {
        DataTable dt = new DataTable(); ;
        DataRow student1 = dt.NewRow();
        DataRow student2 = dt.NewRow();


        dt.Columns.Add("studentid");
        dt.Columns.Add("Name");
        dt.Columns.Add("Decision");
        dt.Columns.Add("Status");
        dt.Columns.Add("MissingItemCodeByBreaks");

        //student 1
        student1["studentid"] = "5";
        student1["Name"] = "Ed Rendell";
        student1["Decision"] = "";
        student1["Status"] = "Incomplete";
        student1["MissingItemCodeByBreaks"] = "Transcript,Standardized Test Scores";

        //student 2
        student2["studentid"] = "6";
        student2["Name"] = "Cole Hamels";
        student2["Decision"] = "Withdrawn By Student Request";
        student2["Status"] = "Incomplete";
        student2["MissingItemCodeByBreaks"] = "Essay,Portfolio,Transcript";
       

        dt.Rows.Add(student1);
        dt.Rows.Add(student2);
   

        return dt;
    }
    public static DataTable allStudents()
    {
        DataTable dt = new DataTable(); ;
        DataRow student1 = dt.NewRow();
        DataRow student2 = dt.NewRow();
        DataRow student3 = dt.NewRow();
        DataRow student4 = dt.NewRow();
        DataRow student5 = dt.NewRow();
        DataRow student6 = dt.NewRow();

        dt.Columns.Add("studentid");
        dt.Columns.Add("Name");
        dt.Columns.Add("Decision");
        dt.Columns.Add("Status");
        dt.Columns.Add("MissingItemCodeByBreaks"); 

        //student 1
        student1["studentid"] = "1";
        student1["Name"] = "Susan Adams";
        student1["Decision"] = "Accept";
        student1["Status"] = "Complete";
        student1["MissingItemCodeByBreaks"] = "";

        //student 2
        student2["studentid"] = "2";
        student2["Name"] = "John Smith";
        student2["Decision"] = "Withdrawn By Student Request";
        student2["Status"] = "Complete";
        student2["MissingItemCodeByBreaks"] = "";

        //student 3
        student3["studentid"] = "3";
        student3["Name"] = "Mike Jordan";
        student3["Decision"] = "Denied";
        student3["Status"] = "Complete";
        student3["MissingItemCodeByBreaks"] = "";

        //student 4
        student4["studentid"] = "4";
        student4["Name"] = "Barack Obama";
        student4["Decision"] = "Confirmed";
        student4["Status"] = "Complete";
        student4["MissingItemCodeByBreaks"] = "";

        //student 5
        student5["studentid"] = "5";
        student5["Name"] = "Ed Rendell";
        student5["Decision"] = "";
        student5["Status"] = "Incomplete";
        student5["MissingItemCodeByBreaks"] = "Transcript,Standardized Test Scores";

        //student 6
        student6["studentid"] = "6";
        student6["Name"] = "Cole Hamels";
        student6["Decision"] = "Withdrawn By Student Request";
        student6["Status"] = "Incomplete";
        student6["MissingItemCodeByBreaks"] = "Essay,Portfolio,Transcript";

        dt.Rows.Add(student1);
        dt.Rows.Add(student2);
        dt.Rows.Add(student3);
        dt.Rows.Add(student4);
        dt.Rows.Add(student5);
        dt.Rows.Add(student6);

        return dt;
    }
}
