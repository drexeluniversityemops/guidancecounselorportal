﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="DrexelAccess.aspx.cs" Inherits="DrexelAccess" %>

<%@ Register src="../linkBar.ascx" tagname="linkBar" tagprefix="uc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Drexel Guidance Counselor Portal</title>
    <style type="text/css">

.Header {
	BACKGROUND-COLOR: #666699;
	width:100%;
}
.Rule{
	BACKGROUND-COLOR: #ffd253;
	height:3px;
}
.Toolbar
{
	FONT-SIZE: 10px; COLOR: black; 
	FONT-FAMILY: verdana, helvetica, arial, sans-serif;
	vertical-align:top;
}
            
BODY {
	COLOR: #000000; FONT-FAMILY: verdana, helvetica, arial, sans-serif;
	margin-left:0px;
	margin-top:0px;
	margin-right:0px;
	margin-bottom:0px;
}
    </style>
</head>
<body>
    <form id="form2" runat="server">
        <div id="header" class="Header">
            <img src="../images/header_new.jpg" alt="Drexel Guidance Counselor Portal"/>
        </div>
        <div id="rule" class="Rule">&nbsp;
        </div>
        <div id="toolbar" class="Toolbar">
            <table>
                <tr>
                    <td>
                        <asp:Label ID="lblWelcome" runat="server"></asp:Label>
                    </td>
                </tr>
            </table>
            
        </div>
    <br />
    <asp:Label runat="server" ID="lblNumofRequests" Font-Bold="true" ForeColor="Red"/>
    &nbsp;-
        <asp:LinkButton ID="lbReports" runat="server" onclick="lbReports_Click">Reports</asp:LinkButton>
    <br />
    <asp:GridView ID="gvRequests" runat="server" AutoGenerateColumns="false"  OnRowCommand="handleRequest">
        <Columns>
            <asp:BoundField HeaderText="ID" DataField="userid" />
            <asp:BoundField HeaderText="First Name" DataField="userFirstName" />
            <asp:BoundField HeaderText="Last Name" DataField="userLastName" />
            <asp:BoundField HeaderText="Email" DataField="userEmail" />
            <asp:BoundField HeaderText="Phone" DataField="userPhone" />
            <asp:BoundField HeaderText="Best Time To Contact" DataField="userBestTimeToContact" />
            <asp:ButtonField CommandName="grant"  Text="Grant Access" ButtonType="Button"/>
            <asp:ButtonField CommandName="deny" Text="Deny Access" ButtonType="Button"/>
        </Columns>
    </asp:GridView>
    </form>
   
</body>
</html>
