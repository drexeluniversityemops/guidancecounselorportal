﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

///ih 12/20/12 created class
/// <summary>
/// Summary description for HighSchool
/// </summary>
public class HighSchool
{
    private SQL sql = new SQL();
    public HighSchool(){}


    public DataTable GetHighSchoolWithUsers()
    {
        return sql.GetHighSchoolsWithUsers();
    }

    public DataSet GetHighSchoolUsers(int highschoolid)
    {
        return sql.GetHighSchoolUsers(highschoolid);
    }

}
