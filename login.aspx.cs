﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Text.RegularExpressions;

public partial class login : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void Submit_Click(object sender, ImageClickEventArgs e)
    {
        String ip = Regex.Replace(Request.ServerVariables["REMOTE_HOST"], @"[0-9]+\.[0-9]+$", "*.*");
        //Response.Write(ip);
        AuthenticateUser(txtEmail.Text, txtPassword.Text, ip);
    }
    private void AuthenticateUser(string email, string password, string ip)
    {
        User currUser = new User(email, password, ip);

        if (currUser.UserID == null)
        {
            errorMessage.Text = "Login Failed, Please try again";
        }
        else
        {
            //ds 10/3/14 - update when user last logged in
            int userID = Convert.ToInt32(currUser.UserID);
            SQL sql = new SQL();
            sql.UpdateUserLastLogin(userID);


            Session["Auth"] = true;
            Session["CurrUser"] = currUser;
            if (currUser.Reset == "True")
            {
                Response.Redirect("changePassword.aspx");
            }
            else
            {
                Response.Redirect("main.aspx");
            }

        }
    }
}
