﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="login.aspx.cs" Inherits="login" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link href="styles/GuidanceConselor.css" rel="stylesheet" type="text/css" />
    <title>Drexel Admissions Update</title>
    <link href="styles/GuidanceConselor.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
        <div id="Header" runat="server">
            <table style="width:100%;background-color:#666699;" >
                <tr>
                    <td>
                        <img src="images/header_new.jpg" alt="logo" />
                    </td>
                </tr>
            </table>
        </div>
        <div id="NavBar">
            <table style="width:100%;">
                <tr>
                    <td width="90%">
                    </td>
                    <td>
                        <div align="center">
                            <a href="default.aspx" ><img height="26" alt="help"  src="images/help_dft.gif" width="39" border="0" name="help" />
                            <br />
							<span class="uportal-label">help</span>
							</a>
					    </div>
                    </td>
                </tr>
            </table>
        </div>
        <div id="Body" runat="server" style="padding-left:20px;">
        <table  width="800px">
            <tr>
                <td>
                    <table class="bgslight" cellspacing="0" cellpadding="4" width="225px" border="0">
					    <tr>
						    <td valign="top" align="center" colspan="2">
						        <span class="text12">
                                    <img style="CURSOR: hand" onclick="javascript:window.open('default.aspx',null,'menubar=no,toolbar=no,titlebar=yes,status=yes,scrollbars=yes,resizable=yes,height=200,width=400');" height="15" alt="DrexelOne Security Notice" hspace="3" src="images/ssl_lock.gif" width="12" align="left" border="0" />
                                    <b>Secure Access Login</b>
                                </span>
                            </td>
						</tr>
						
						<tr>
							<td align="right">
							    <span class="text10">
							        <b>
							       
							        Email:
							        </b>
							    </span>
							</td>
							<td>
							    <span class="text10">
							        <asp:textbox id="txtEmail" runat="server" ToolTip="Type your E-mail address" Width="125px"></asp:textbox><asp:RequiredFieldValidator id="RequiredFieldValidatoremail" Text="*" Display="Dynamic" runat="server" ErrorMessage="Email required" ControlToValidate="txtEmail" /><asp:RegularExpressionValidator id="RegularExpressionValidatorEMail" runat="server" Text="*" Display="Dynamic" ErrorMessage="Email address not in correct format" ControlToValidate="txtEmail" ValidationExpression="^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$"></asp:RegularExpressionValidator>
								</span>
							</td>
						</tr>
						<tr>
							<td align="right">
							<span class="text10">
							    <b>
							    
							    
							    Password:
							    </b>
							</span>
							</td>
							<td>
							    <span class="text10">
							        <asp:textbox id="txtPassword" runat="server" TextMode="Password" ToolTip="Type your password" Width="125px" ></asp:textbox><asp:RequiredFieldValidator id="RequiredFieldValidatorpassword" Text="*" Display="Dynamic" runat="server" ErrorMessage="Password required" ControlToValidate="txtPassword" />
								</span>
                            </td>
						</tr>
						<tr>
						    <td colspan="2">
						        <span class="text10">
						            <b>
						            <asp:ValidationSummary HeaderText="There are problems with the following fields:" runat="server" CssClass="text10" id="ValidationSummary1" />
						            </b>
						        </span>
						    </td>
						</tr>
						<tr>
						    <td align="center" colspan="2">
						        <asp:ImageButton id="Submit" runat="server" AlternateText="Login" 
                                    ImageUrl="images/login.gif" ToolTip="Click here to login" 
                                    onclick="Submit_Click"></asp:ImageButton>
						        <asp:ImageButton id="Cancel" runat="server" AlternateText="Cancel" ImageUrl="images/cancel.gif" ToolTip="Click here to reset"></asp:ImageButton>
						    </td>
						</tr>
					    <tr>
						    <td colspan="2">
						        <asp:Label CssClass="text10" ForeColor="#ff0000" ID="errorMessage" Runat="server"></asp:Label>
						    </td>
						</tr>
						<tr>
						    <td align="center" colspan="2">
						        <span class="text10">
						            <a href="default.aspx">Having problems logging in?</a>
						            <br />
							    </span>
						    </td>
						</tr>
					</table>
				    <table  cellspacing="0" width="100%">
				        <tr class="WhiteSpace"></tr>
				        <tr>
				            <td class="SectionHeader">&nbsp;</td>
				        </tr>
				        <tr>
				            <td class="bgs"><span class="text12"><a href="register.aspx">Register Your School</a></span></td>
				        </tr>
				        <tr>
				            <td class="bgs"><span class="text12"><a href="requestAccess.aspx">Request User Name & Password</a></span></td>
				        </tr>
				        <tr class="WhiteSpace"></tr>
				        <tr>
				            <td class="SectionHeader">&nbsp;</td>
				        </tr>
				        <tr class="bgs">
				            <td>
				                <span class="text10">
							    <a href="http://www.drexel.edu/em/undergrad/about/" target="_blank">About Drexel</a><br>
							    <a href="http://www.drexel.edu/em/undergrad/academics/" target="_blank">Academic Programs</a><br>
							    <a href="http://www.drexel.edu/em/undergrad/coop/" target="_blank">Drexel Co-op</a><br>
							    <a href="http://www.drexel.edu/em/undergrad/financialaid/" target="_blank">Financing Options</a><br>
							    <a href="http://www.drexel.edu/em/undergrad/studentlife/" target="_blank">Student Life</a><br>
							    <a href="http://www.drexel.edu/admissions/visiting.aspx" target="_blank">Visiting Drexel</a><br>
							    <a href="http://www.drexel.edu/em/apply/undergrad/default.html" target="_blank">How to Apply</a><br>
							    <a href="http://www.drexel.edu/em/contact/" target="_blank">Contact Admissions</a>
							    </span>
							</td>
				        </tr>
				    </table>		
				</td>
				<td> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </td>   
                <td valign="top" align="center" style="width:100%;">
                
                    <table align="center" style="text-align:left;width:500px;width:100%;">
				        <tr>
				            <td class="SectionHeader">&nbsp;
				            </td>
				        </tr>
				        <tr>
				            <td class="uportal-head14-bold">Welcome to Drexel Universtity</td>
				        </tr>
				        <tr>
				            <td class="uportal-channel-text">
				                The Admissions Office has developed this website so you can quickly check on the 
				                status of your students who are applying to Drexel. With the Drexel Admissions Update Website 
								you'll be the first to learn about what's happening at Drexel. If you need help 
								using this website, select <a href="default.aspx" target="_blank">
								Help</a> on the top right corner of your page.
				            </td>
				        </tr>
				        <tr>
				            <td class="WhiteSpace"></td>
				        </tr>
				        <tr>
				            <td class="SectionHeader">&nbsp;</td>
				        </tr>
				        <tr>
				            <td class="uportal-head14-bold">What's Inside?</td>
				        </tr>
				        <tr>
				            <td class="uportal-channel-text">
				                <table cellspacing="0" cellpadding="0" width="100%" border="0">
								    <tr>
								        <td>
								            <span class="text10"><B>Upcoming Events:</B> Find out when the next open houses and information sessions will be held.</SPAN>
								        </td>
								    </tr>
								    <tr>
								        <td>
								            <img height="10" src="DrexelOne - powered by Campus Pipeline_files/dot-blank.gif" width="1">
								        </td>
								    </tr>
								    <tr>
								        <td>
								            <span class="text10"><B>View Student Status:</B> Check on the status of your students' applications.</span>
								        </td>
								    </tr>
								    <tr>
								        <td>
								            <img height="10" src="DrexelOne - powered by Campus Pipeline_files/dot-blank.gif" width="1">
								        </td>
								    </tr>
								    <tr>
								        <td>
								            <span class="text10"><B>Drexel Updates for Your High School:</B> See what's new in Admissions.</span>
								        </td>
								    </tr>
								    <tr>
								        <td>
								            <img height="20" src="DrexelOne - powered by Campus Pipeline_files/dot-blank.gif" width="1">
								        </td>
								    </tr>
                                </table>
				            </td>
				        </tr>
				    </table>
                </td>
               
                <td width="100px">
                </td>
            </tr>
            
        </table>
      </div>
    </form>
</body>
</html>
