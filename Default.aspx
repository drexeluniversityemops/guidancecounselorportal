﻿<%@ Page Language="C#" AutoEventWireup="true"  CodeFile="Default.aspx.cs" Inherits="_Default" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<%@ Register src="linkBar.ascx" tagname="linkBar" tagprefix="uc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">

    <link href="styles/GuidanceConselor.css" rel="stylesheet" type="text/css" />

    <title>Drexel Admissions Update</title>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
        <div id="header" >
            <img src="images/header_new.jpg" alt="Drexel Guidance Counselor Portal"/>
        </div>
        <div id="rule" class="Rule">&nbsp;
        </div>
        <div id="toolbar" class="Toolbar">
            <table>
                <tr>
                    
                    <td><uc1:linkBar ID="linkBar1" runat="server" />
                    </td>
                </tr>
            </table>
            
        </div>
        <div id="body">
            <table width="800px" align="center">
                <tr>
                    <td class="RegularText">
                       <h1>Drexel Admissions Update - Guidance Counselor Portal</h1>

                        The Admissions Department at Drexel brings you the Guidance Counselor Portal (GCP)
                         so you can aid students who have applied to Drexel University.  This site lets you 
                         track your students’ status and ensure that they have submitted all the requirements 
                         to complete an application.  We will also keep you informed of any events here at the
                          University.  Please use the following links to answer any questions that you have:

                    </td>
                </tr>
                <tr>
                    <td>
                        <cc1:Accordion ID="Accordion1" runat="server"  HeaderCssClass="accordionHeader" ContentCssClass="accordionContent" FadeTransitions="true" FramesPerSecond="40" TransitionDuration="250" AutoSize="None" SelectedIndex="0">
                            <Panes>
                                <cc1:AccordionPane ID="AccordionPane5" runat="server">
                                    <Header>
                                        <a href="" onclick="return false;" class="accordionLink"></a>
                                    </Header>
                                    <Content>
                                        
                                    </Content>
                                </cc1:AccordionPane>
                                <cc1:AccordionPane ID="AccordionPane1" runat="server">
                                    <Header>
                                        <a href="" onclick="return false;" class="accordionLink">How can my school register?</a>
                                    </Header>
                                    <Content>
                                        In order for counselors from you school to gain access to the site, you must <a href="register.aspx" >register your school</a> and designate a representative to act as an administrator.  This person will be in charge of granting access to other counselors from your school.  </a>
                                    </Content>
                                </cc1:AccordionPane>
                                <cc1:AccordionPane ID="AccordionPane2" runat="server">
                                    <Header>
                                        <a href="" onclick="return false;" class="accordionLink">How can a Guidance Counselor from my school gain access to the portal?</a>
                                    </Header>
                                    <Content>
                                        After your <a href="register.aspx">school is registered in the system</a>,  the designated representative is in charge of granting access through the GCP administrative tools available after they <a href="main.aspx">login</a>.  Individual counselors can request access  through the <a href="requestAccess.aspx">Counselor Access Request Page</a>. 
                                    </Content>
                                </cc1:AccordionPane>
                                <cc1:AccordionPane ID="AccordionPane3" runat="server">
                                    <Header>
                                        <a href="" onclick="return false;" class="accordionLink">How can I be sure that access is secure?</a>
                                    </Header>
                                    <Content>
                                        All schools are contacted to ensure the identity of the person requiring administrative access.  That person is then in charge of granting access to the counselors in your school ensuring that every user’s identity is known.  Second, we keep transactions of every login from every user on the site allowing us to track any suspicious activity.  Finally, all data transactions are encrypted ensuring safe data transfer.
                                    </Content>
                                </cc1:AccordionPane>
                                <cc1:AccordionPane ID="AccordionPane4" runat="server">
                                    <Header>
                                        <a href="" onclick="return false;" class="accordionLink">I am having trouble either registering for access or logging into the portal. Who can I contact?</a>
                                    </Header>
                                    <Content>
                                        The system should give you a message describing the problem with your submission. Please note it and then contact <asp:Label runat="server" ID="lblContactName" /> at <asp:Label runat="server" ID="lblContactNumber" /> or <asp:Label runat="server" ID="lblContactEmail"/> 
                                    </Content>
                                </cc1:AccordionPane>
                            </Panes> 
                        </cc1:Accordion>
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
