using System;
using System.Data;


	/// <summary>
	/// Summary description for Announcement.
	/// </summary>
	public class Announcement
	{
		#region Private Variables
		int _AnnouncementID;
		string _AnnouncementTitle;
		string _AnnouncementBody;
		#endregion

		#region Properties
		public int AnnouncementID
		{
			get{return _AnnouncementID;}
			set{_AnnouncementID = value;}
		}
		public string AnnouncementTitle
		{
			get{return _AnnouncementTitle;}
			set{_AnnouncementTitle = value;}
		}
		public string AnnouncementBody
		{
			get{return _AnnouncementBody;}
			set{_AnnouncementBody = value;}
		}
		#endregion

		#region Constructors
		public Announcement(int announcementID, string announcementTitle)
		{
			AnnouncementID = announcementID;
			AnnouncementTitle = announcementTitle;
			AnnouncementBody = null;
		}
		public Announcement(string Id)
		{
			DataSet ds = new DataSet();
			SQL data = new SQL();
			ds.Tables.Add(data.getAnnouncementBody(Id));
			AnnouncementID = 0;
			AnnouncementTitle = ds.Tables["Announcement"].Rows[0]["announcementhead"].ToString();
			AnnouncementBody = ds.Tables["Announcement"].Rows[0]["announcementtext"].ToString();
		}
		#endregion
	}

