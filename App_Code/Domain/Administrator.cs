using System;
using System.Data;


	/// <summary>
	/// Summary description for Administrator.
	/// </summary>
	public class Administrator
	{
		#region Private Variables
		string _FirstName;
		string _LastName;
		string _Email;
		#endregion

		#region Properties
		public string FirstName
		{
			get{return  _FirstName;}
			set{_FirstName = value;}
		}
		public string LastName
		{
			get{return _LastName;}
			set{_LastName =value;}
		}
		public string Email
		{
			get{return _Email;}
			set{_Email = value;}
		}
		#endregion

		#region Constuctors
		public Administrator(string ceeb)
		{
			getAdminInfo(ceeb);
		}
		#endregion

		#region Methods
		void getAdminInfo(string ceeb)
		{
            if (ceeb == "7777777")
            {
                FirstName = "Your School";
                LastName = "Admin";
                Email = "enroll@drexel.edu";
            }
            else
            {
                try
                {
                    SQL data = new SQL();
                    DataSet ds = new DataSet();
                    ds.Tables.Add(data.getSchoolAdminInfo(ceeb));
                    FirstName = ds.Tables["SchoolAdmin"].Rows[0]["userFirstName"].ToString();
                    LastName = ds.Tables["SchoolAdmin"].Rows[0]["userLastName"].ToString();
                    Email = ds.Tables["SchoolAdmin"].Rows[0]["userEmail"].ToString();
                }
                catch
                {
                    FirstName = null;
                    LastName = null;
                    Email = null;
                }
            }
			#endregion	

		}
	}

