﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class tools_RequestReport : System.Web.UI.Page
{
    requestAccessHandler handler = new requestAccessHandler();
    User user;
    protected void Page_Load(object sender, EventArgs e)
    {
        gvRequestReport.DataSource = handler.getUserRequestReport();
        gvRequestReport.DataBind();
    }
    private void validateUser()
    {
        //Check for session object
        if (Session["GC_User"] != null)
        {
            //make session object current user object
            user = (User)Session["GC_User"];
            //verify user is in database
            if (handler.checkValidatedUserAgainstDB(user) != true)
            {
                //redirect to admin login.
                Session["GC_User"] = null;
                Response.Redirect("admin.aspx?error=true");
            }
        }
        else
        {
            //there is no session object, redirect to login
            Response.Redirect("admin.aspx?error=true");
        }

    }
}
