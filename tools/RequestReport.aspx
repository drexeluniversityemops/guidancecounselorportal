﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="RequestReport.aspx.cs" Inherits="tools_RequestReport" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Drexel Guidance Counselor Portal</title>
    <style type="text/css">

.Header {
	BACKGROUND-COLOR: #666699;
	width:100%;
}
.Rule{
	BACKGROUND-COLOR: #ffd253;
	height:3px;
}
.Toolbar
{
	FONT-SIZE: 10px; COLOR: black; 
	FONT-FAMILY: verdana, helvetica, arial, sans-serif;
	vertical-align:top;
}
            
BODY {
	COLOR: #000000; FONT-FAMILY: verdana, helvetica, arial, sans-serif;
	margin-left:0px;
	margin-top:0px;
	margin-right:0px;
	margin-bottom:0px;
}
        .style1
        {
            FONT-SIZE: 10px;
            COLOR: black;
            FONT-FAMILY: verdana, helvetica, arial, sans-serif;
            vertical-align: top;
            font-weight: bold;
        }
        .style2
        {
            font-size: medium;
        }
    </style>
</head>
<body>
    <form id="form2" runat="server">
        <div id="header" class="Header">
            <img src="../images/header_new.jpg" alt="Drexel Guidance Counselor Portal"/>
        </div>
        <div id="rule" class="Rule">&nbsp;
        </div>
        <div id="toolbar" class="style1">
            <table>
                <tr>
                    <td class="style2">
                        Request Report</td>
                </tr>
            </table>
        </div>
            <table align="center">
                <tr>
                    <td><asp:GridView ID="gvRequestReport" runat="server" AutoGenerateColumns="false" AlternatingRowStyle-BackColor="#efefef" HeaderStyle-BackColor="Navy" HeaderStyle-ForeColor="White" Font-Size="Small">
                        <Columns>
                            <asp:BoundField DataField="userName" HeaderText="Name"/>
                            <asp:BoundField DataField="Admin" HeaderText="isAdmin"/>
                            <asp:BoundField DataField="RequestGranted" HeaderText="Granted Access"/>
                            <asp:BoundField DataField="userEmail" HeaderText="Email"/>
                            <asp:BoundField DataField="SchoolID" HeaderText="Ceeb"/>
                            <asp:BoundField DataField="HighSchoolName" HeaderText="School Name"/>
                            <asp:BoundField DataField="newLogin" HeaderText="New User"/>
                            <asp:BoundField DataField="CounselorName" HeaderText="Drexel Contact"/>
                            <asp:BoundField DataField="CounselorPhone" HeaderText="Phone"/>
                            <asp:BoundField DataField="CounselorEmail" HeaderText="Email"/>
                        </Columns>
                    </asp:GridView>
                    </td>
                </tr>
                
            </table>
            
            
     

    </form>
   
</body>
</html>

