﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="EditIpAddress.aspx.cs" Inherits="EditIpAddress" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Admin</title>
    <link href="styles/GuidanceConselor.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div>
        <div id="header" class="Header">
            <img src="images/header_new.jpg" alt="Drexel Guidance Counselor Portal"/>
        </div>
        <div id="rule" class="Rule">&nbsp;
        </div>
        <div id="toolbar" class="Toolbar">
            <table width="100%";>
                <tr>
                    <td style="text-align:left;">
                        <asp:Label ID="lblWelcome" runat="server"></asp:Label>
                    </td>
                    <td style="text-align:right;">
                    <asp:LinkButton ID="lbBack" runat="server" PostBackUrl="~/main.aspx"><< Back to Main Page</asp:LinkButton>
                    </td>
                </tr>
            </table>
            
        </div>

        Please select a school: <asp:DropDownList runat="server" ID="ddlSchool" AutoPostBack="true" OnSelectedIndexChanged="ddlSchool_OnSelectedIndexChanged"></asp:DropDownList>    
        <br /><br />
        <asp:UpdatePanel runat="server" id="UpdatePanel" Visible="false">
            <ContentTemplate>
                <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="false">
                    <Columns>
                        <asp:BoundField HeaderText="User ID" DataField="userID" />
                        <asp:BoundField HeaderText="First Name" DataField="userFirstName" />
                        <asp:BoundField HeaderText="Last Name" DataField="userLastName" />
                        <asp:BoundField HeaderText="Email Address" DataField="userEmail" />
                        <asp:BoundField HeaderText="Phone Number" DataField="userPhone" />
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:Button runat="server" ID="btnPasswordReset" OnClick="btnPasswordReset_OnClick" Text="Reset Password" /><asp:HiddenField runat="server" ID="hfID" Value='<%# Eval("userID") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
                <br />
                Ip Address <asp:TextBox runat="server" ID="txtIpAddress"></asp:TextBox><asp:Button runat="server" ID="btnUpdateIpAddress" OnClick= "btnUpdateIpAddress_OnClick" Text="Update Ip Addresses" />
                <br />
                <asp:Label runat="server" ID="lblUpdated" Font-Bold="true" ForeColor="Red" Visible="false"></asp:Label>
                <asp:HiddenField runat="server" ID="hfIp" />
            </ContentTemplate>        
        </asp:UpdatePanel>
    </div>
    </form>
</body>
</html>
