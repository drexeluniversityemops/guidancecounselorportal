﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="events.ascx.cs" Inherits="Controls_events" %>
<asp:Repeater ID="rptCategories" runat="server">
	<ItemTemplate>
		<table border="0" cellpadding="0" cellspacing="0" bordercolor="#CCCCCC" class="copy">
			<tr valign="top">
				<td colspan="4" class="uportal-channel-text"><b><a name="<%# DataBinder.Eval(Container.DataItem, "EventTypeID")%>"></a><%# DataBinder.Eval(Container.DataItem, "EventTypeName")%></b>
				</td>
			</tr>
		</table>
		<asp:DataGrid DataSource='<%# CheckForEvents( Convert.ToInt32(DataBinder.Eval(Container.DataItem, "EventTypeID")))%>' AutoGenerateColumns="False" ShowHeader="false" CellPadding="3" cellspacing="0" ItemStyle-CssClass="uportal-channel-text" CssClass="uportal-channel-text" BorderColor="#CCCCCC" Border="0" Runat="server" ID="myDataView">
			<Columns>
				<asp:BoundColumn DataField="EventTypeEmpty" ItemStyle-VerticalAlign="Top" />
			</Columns>
		</asp:DataGrid>					
		<asp:DataGrid DataSource='<%# FilterEvents( Convert.ToInt32(DataBinder.Eval(Container.DataItem, "EventTypeID")) )%>' AutoGenerateColumns="False" Width="100%" ShowHeader="false" CellPadding="3" cellspacing="0" ItemStyle-CssClass="uportal-channel-text" CssClass="uportal-channel-text" BorderColor="#CCCCCC" Border="1" Runat="server" ID="myDataView2">
			<Columns>
				<asp:BoundColumn DataField="EventCollegeProgram" ItemStyle-VerticalAlign="Top" />							
				<asp:HyperLinkColumn Target ="_blank" DataTextField="EventStart" DataTextFormatString="{0:ddd., MMMM d}" DataNavigateUrlField="EventID" DataNavigateUrlFormatString="http://deptapp1.drexel.edu/em/events/detail.aspx?ID={0}"  ItemStyle-Width="120px" ItemStyle-VerticalAlign="Top" />
			</Columns>
		</asp:DataGrid>
		<BR>
	</ItemTemplate>				
</asp:Repeater>