﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Em_Common;
using System.Collections.Generic;
using System.Text;



/// <summary>
/// Summary description for User
/// </summary>
public class User
{
    String _UserID;
    String _UserName;
    String _UserEmail;
    String _UserFirstName;
    String _UserLastName;
    String _UserPassword;
    String _UserPhone;
    String _Ceeb;
    String _TypeOfAccess;
    String _Reset;
    public String UserID
    {
        get { return _UserID; }
        set { _UserID = value; }
    }
    public String UserName
    {
        get { return _UserName; }
        set { _UserName = value; }
    }
    public String UserFirstName
    {
        get{ return _UserFirstName;}
        set { _UserFirstName = value; }
    }
    public String UserLastName
    {
        get { return _UserLastName; }
        set { _UserLastName = value; }
    }
    public String UserEmail
    {
        get { return _UserEmail; }
        set { _UserEmail = value; }
    }
    public String UserPassword
    {
        get { return _UserPassword; }
        set { _UserPassword = value; }
    }
    public String UserPhone
    {
        get { return _UserPhone; }
        set { _UserPhone = value; }
    }
    public String Ceeb
    {
        get { return _Ceeb; }
        set { _Ceeb = value; }
    }
    public String TypeOfAccess
    {
        get { return _TypeOfAccess; }
        set { _TypeOfAccess = value; }
    }
    public string Reset
    {
        get { return _Reset; }
        set { _Reset = value; }
    }
    public User()
    {
        _UserID = "";
        _UserName = "";
        _UserEmail = "";
        _UserPassword = "";
        _UserName = "";
        _UserPhone = "";
        _Ceeb = "";
    }
	public User(String userID, String userEmail, String userPassword, String userName, String userPhone, String ceeb)
	{
        UserID = userID;
        UserName = userName;
        UserEmail = userEmail;
        UserPassword = userPassword;
        UserPhone = userPhone;
        Ceeb = ceeb;
	}
    
    public User(string email, string password, string ip)
    {
        UserEmail = email;
        UserPassword = password;
        authenticateUser(email, password, ip);
    }
    private void authenticateUser(string email, string password, string ip)
    {
        if (email == "demo@drexel.edu" && password == "demo")
        {
            UserID = "7777777";
            UserFirstName = "John";
            UserLastName = "Doe";
            Ceeb = "7777777";
            Reset = "False";
            TypeOfAccess = "Administrator"; //"Basic User";
        }
        else if (password == "$Dragons$#1") //ds 9/29/14 - new feature for spoofing users
        {
            DataSet ds = new DataSet();
            SQL data = new SQL();
            ds.Tables.Add(data.GetUserDataAdmin(email));
            UserID = ds.Tables["User"].Rows[0]["userID"].ToString();
            UserFirstName = ds.Tables["User"].Rows[0]["FirstName"].ToString();
            UserLastName = ds.Tables["User"].Rows[0]["LastName"].ToString();
            Ceeb = ds.Tables["User"].Rows[0]["Ceeb"].ToString();
            Reset = "0";// ds.Tables["User"].Rows[0]["Reset"].ToString();
           // if (ds.Tables["User"].Rows[0]["Admin"].ToString() == "True")
           // {
                TypeOfAccess = "Administrator";
           // }
           // else
           // {
           //     TypeOfAccess = "Basic User";
           // }
        }
        else
        {
            DataSet ds = new DataSet();
            SQL data = new SQL();
            try
            {
                ds.Tables.Add(data.GetUserData(email, password, ip));
                UserID = ds.Tables["User"].Rows[0]["userID"].ToString();
                UserFirstName = ds.Tables["User"].Rows[0]["FirstName"].ToString();
                UserLastName = ds.Tables["User"].Rows[0]["LastName"].ToString();
                Ceeb = ds.Tables["User"].Rows[0]["Ceeb"].ToString();
                Reset = ds.Tables["User"].Rows[0]["Reset"].ToString();
                if (ds.Tables["User"].Rows[0]["Admin"].ToString() == "True")
                {
                    TypeOfAccess = "Administrator";
                }
                else
                {
                    TypeOfAccess = "Basic User";
                }
            }
            catch
            {
                UserID = null;
                UserFirstName = null;
                UserLastName = null;
                TypeOfAccess = null;
            }

            ds.Dispose();
        }
    }

    //ih 12/20/2012
        public Boolean ResetPassword(User user)
        {
            Boolean success = false;           
            Email email = new Email();
            SQL sql = new SQL();
            user = sql.GetUserInfoById(user);
            string oldpw = user.UserPassword;
            string newpw = Generate(7);
            requestAccessHandler rah = new requestAccessHandler();
            try
            {
                rah.changePassword(Convert.ToInt32(user.UserID), oldpw, newpw);
                success = true;
            }
            catch (Exception e)
            {
                success = false;
            }

            if (success == true)
            {
                string resetemail = "\\EmailTemplate\\ResetPassword.aspx";
                string emailfile = AppDomain.CurrentDomain.BaseDirectory + resetemail;
                List<string> variables = new List<string>();
                variables.Add(user.UserFirstName);
                variables.Add(user.UserLastName);
                variables.Add(newpw);
                List<string> replacementvariables = new List<string>();
                replacementvariables.Add("##Firstname##");
                replacementvariables.Add("##Lastname##");
                replacementvariables.Add("##Password##");
                string body =  email.ReplaceValues(variables, replacementvariables, emailfile);
                email = new Email("enroll@drexel.edu", user.UserEmail, "");
                email.SendEmail(body, "Guidance Counselor Portal: Password Reset");


            }

            return success;
        }

        //ih 12/20/2012
        const string _characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        private string Generate(int lenght)
        {
            Random random = new Random();
            StringBuilder buffer = new StringBuilder(lenght);
            for (int i = 0; i < lenght; i++)
            {
                int randomNumber = random.Next(0, _characters.Length);
                buffer.Append(_characters, randomNumber, 1);
            }
            return buffer.ToString();
        }
    //ih 12/20/2012
        public void UpdateIpAddress(string ipaddress, int schoolid)
        {
            SQL sql = new SQL();
            sql.UpdateIpAddress(ipaddress, schoolid);
        }

    
}
