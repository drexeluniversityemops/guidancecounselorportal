﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Text.RegularExpressions;

public partial class register : System.Web.UI.Page
{
    enum MessageCodes
    {
        Success = 0,
        ExistingEmail = 1,
        ExisitngAdmin = 2,
        NonExistingCeeb = 3,
        ExistingRequest = 4
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        cbLimited.Attributes.Add("OnClick", "jscript.validation.requestAccessCheckBox()");
        cbMisuse.Attributes.Add("OnClick", "jscript.validation.requestAccessCheckBox()");
        btnSubmit.Attributes.Add("OnClick", "jscript.popup.displayPopup()");
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        //btnSubmit.Enabled = false;
        Boolean pageIsValid = validatePage();
        if (pageIsValid == true)
        {
            // panel1.Visible = true;
            requestAccessHandler handler = new requestAccessHandler();
            String IP = Regex.Replace(Request.ServerVariables["REMOTE_HOST"], @"[0-9]+\.[0-9]+$", "*.*");
            int returnCode;

            //Form element display
            pnForm.Visible = false;
            pnResults.Visible = true;
            btnSubmit.Visible = false;

            returnCode = handler.requestAccess(1, IP, txtFirstName.Text, txtLastName.Text, Convert.ToInt32(txtCEEB.Text), txtEmail.Text, txtPhone.Text, ddlContact.SelectedValue);
            if (returnCode == (int)MessageCodes.Success)
            {
                handler.sendNotification(txtFirstName.Text, txtLastName.Text, Convert.ToInt32(txtCEEB.Text));
            }

            displayMessage(returnCode);
            //populate labels
            lblHeaderDesc.Text = "Please review the information below";
            lblFirstName.Text = txtFirstName.Text;
            lblLastName.Text = txtLastName.Text;
            lblCEEB.Text = txtCEEB.Text;
            lblPhone.Text = txtPhone.Text;
            lblEmail.Text = txtEmail.Text;
            lblContact.Text = ddlContact.SelectedValue;
        }



    }
    private void displayMessage(int messageCode)
    {
        switch (messageCode)
        {
            case (int)MessageCodes.Success:
                lblResult.CssClass = "OKMessage";
                lblResult.Text = "You have successfully submitted your request.";
                break;
            case (int)MessageCodes.ExisitngAdmin:
                lblResult.CssClass = "ErrorMessage";
                lblResult.Text = "An Adminstrator is already registered at this school! Please Request Counselor Access.";
                lbEdit.Visible = true;
                break;
            case (int)MessageCodes.ExistingEmail:
                lblResult.CssClass = "ErrorMessage";
                lblResult.Text = "This email is already registered! Please try another.";
                lbEdit.Visible = true;
                break;
            case (int)MessageCodes.NonExistingCeeb:
                lblResult.CssClass = "ErrorMessage";
                lblResult.Text = "This CEEB code appears to be incorrect. Please edit your submission.";
                lbEdit.Visible = true;
                break;
            case (int)MessageCodes.ExistingRequest:
                lblResult.CssClass = "ErrorMessage";
                lblResult.Text = "There is already a Request for the School.";
                lbEdit.Visible = true;
                break;
        }
    }
    protected void lbEdit_Click(object sender, EventArgs e)
    {
        pnForm.Visible = true;
        pnResults.Visible = false;
        btnSubmit.Enabled = true;
        lbEdit.Visible = false;
        btnSubmit.Visible = true;
    }
    public Boolean validatePage()
    {
        Boolean pageValid = true;
        if (txtCEEB.Text == "" || txtCEEB.Text == null)
        {
            lblCeebCodeVal.Visible = true;
            pageValid = false;
        }
        if (txtFirstName.Text == "" || txtFirstName == null)
        {
            lblFirstNameVal.Visible = true;
            pageValid = false;
        }
        if (txtLastName.Text == "" || txtLastName == null)
        {
            lblLastNameVal.Visible = true;
            pageValid = false;

        }
        if (txtPhone.Text == "" || txtPhone.Text == null)
        {
            lblTelephoneVal.Visible = true;
            pageValid = false;
        }
        if (ddlContact.SelectedValue == "" || ddlContact.SelectedValue == null)
        {
            lblContactVal.Visible = true;
            pageValid = false;
        }
        if (txtEmail.Text == "" || txtEmail.Text == null)
        {
            lblEmailVal.Visible = true;
        }

        if (pageValid == true)
        {
            lblCeebCodeVal.Visible = false;
            lblFirstNameVal.Visible = false;
            lblLastNameVal.Visible = false;
            lblTelephoneVal.Visible = false;
            lblEmailVal.Visible = false;
            lblContactVal.Visible = false;
        }
        return pageValid;
    }
}
