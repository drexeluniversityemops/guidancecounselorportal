﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="IncompleteStudents.aspx.cs" Inherits="IncompleteStudents" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Incomplete Students</title>
    <link href="styles/GuidanceConselor.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div id="Header" runat="server">
            <table style="width:100%;background-color:#666699;" >
                <tr>
                    <td>
                        <img src="images/header_new.jpg" alt="logo" />
                    </td>
                </tr>
            </table>
        </div>
        <div id="NavBar">
            <table style="width:100%;">
                <tr>
                    <td width="80%" style="padding:20px;">
                        <asp:Label ID="lblHello" runat="server" Text=""></asp:Label>
                    </td>
                    <td>
                        <asp:LinkButton runat="server" Text="<< Back To Main Page" ID="lbBack" PostBackUrl="~/main.aspx" CssClass="uportal-channel-text"/>
                    </td>
                    <td>
                        <div align="center">
                            <a href="default.aspx" ><img height="26" alt="help"  src="images/help_dft.gif" width="39" border="0" name="help" />
                            <br />
							<span class="uportal-label">help</span>
							</a>
					    </div>
                    </td>
                </tr>
            </table>
        </div>
    <div>
        <table  cellpadding="4" cellspacing="0" width="75%" style="PADDING-LEFT:20px;BORDER-RIGHT: none; BORDER-TOP: none; BORDER-LEFT: none; BORDER-BOTTOM: none;" >
					<tr>
						<td colspan="4"><asp:label id=CompOrInCompLabel Runat="server" Font-Bold="True" ForeColor="Black" CssClass="uportal-head14-bold"></asp:label></td>
					</tr>	
					<tr>
						<td colspan="4"><asp:label id=TextForCompInComp Runat="server" Font-Bold="False" ForeColor="Black" CssClass="uportal-text-small"></asp:label></td>
					</tr>	
					<tr>
						<td colspan="4"><asp:label id=LabelLastUpload Runat="server" Font-Bold="False" ForeColor="Black" CssClass="uportal-text-small"></asp:label></td>
					</tr>
				<asp:Repeater ID="rpStudentList" Runat="server">
					<HeaderTemplate>
						<tr >
							<td class="uportal-text-small" style="BORDER-BOTTOM: black 1px solid;"><font color="blue"><b>Student Name</b></font></td>
							<td class="uportal-text-small" style="BORDER-BOTTOM: black 1px solid"><font color="blue"><b>Application Status</b></font</td>
							<td class="uportal-text-small" style="BORDER-BOTTOM: black 1px solid"><font color="blue"><b>Application Materials Needed</b></font</td>
						</tr>
					</HeaderTemplate>
					<ItemTemplate>
						<tr >
							<td class="uportal-text-small" style="BORDER-RIGHT: black 1px solid;">
								<a href="StudentInfo.aspx?id=<%# DataBinder.Eval(Container.DataItem, "studentid") %>"><%# DataBinder.Eval(Container.DataItem, "Name") %></a>&nbsp;
							</td>
							<td class="uportal-text-small" style="BORDER-RIGHT: black 1px solid"><%# DataBinder.Eval(Container.DataItem, "Status") %>&nbsp;
							</td>
							<td class="uportal-text-small" ><%# DataBinder.Eval(Container.DataItem, "MissingItemCodeByBreaks")%>&nbsp;
							</td>
						</tr>
					</ItemTemplate>
					<AlternatingItemTemplate>
						<tr bgcolor="#efefef" style="BORDER-RIGHT: black 1px solid">
							<td class="uportal-text-small" style="BORDER-RIGHT: black 1px solid">
								<a href="StudentInfo.aspx?id=<%# DataBinder.Eval(Container.DataItem, "studentid") %>"><%# DataBinder.Eval(Container.DataItem, "Name") %></a>&nbsp;
							</td>
							<td class="uportal-text-small" style="BORDER-RIGHT: black 1px solid"><%# DataBinder.Eval(Container.DataItem, "Status") %>&nbsp;
							</td>
							<td class="uportal-text-small" ><%# DataBinder.Eval(Container.DataItem, "MissingItemCodeByBreaks")%>&nbsp;
							</td>
						</tr>
					</AlternatingItemTemplate>
				</asp:Repeater>
			</table>
    </div>
    </form>
</body>
</html>
