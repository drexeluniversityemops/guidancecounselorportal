using System;
using System.Data;


	/// <summary>
	/// Summary description for Counselor.
	/// </summary>
	public class Counselor
	{
		#region Private Variables

		string _FirstName;
		string _LastName;
		string _Address;
		string _City;
		string _State;
		string _Zip;
		string _Phone;
		string _Fax;
		string _Email;
		string _Ext;
		string _Title;
		string _UserName;

		#endregion

		#region Properties

		public string FirstName
		{
			get{return _FirstName;}
			set{_FirstName = value;}
		}
		public string LastName
		{
			get{return _LastName;}
			set{_LastName = value;}
		}
		public string Title
		{
			get{return _Title;}
			set{_Title = value;}
		}
		public string Address
		{
			get{return _Address;}
			set{_Address = value;}
		}
		public string City
		{
			get{return _City;}
			set{_City = value;}
		}
		public string State
		{
			get{return _State;}
			set{_State = value;}
		}
		public string Zip
		{
			get{return _Zip;}
			set{_Zip = value;}
		}
		public string Phone
		{
			get{return _Phone;}
			set{_Phone = value;}
		}
		public string Fax
		{
			get{return _Fax;}
			set{_Fax = value;}
		}
		public string Email
		{
			get{return _Email;}
			set{_Email = value;}

		}
		public string Ext
		{
			get{return _Ext;}
			set{_Ext = value;}
		}
		public string UserName
		{
			get{return _UserName;}
			set{_UserName = value;}
		}

		#endregion

		#region Constructor
		public Counselor(string ceeb)
		{
			getCounselorByCeeb(ceeb);
		}
		#endregion

		#region Methods

		public void getCounselorByCeeb(string ceeb)
		{
			//instansiate  objects
			SQL data = new SQL();
			DataSet ds = new DataSet();
			//filll dataset
			ds.Tables.Add(data.getTerritoryManager(ceeb));
			
			//set Properties
            if (ceeb == "7777777")
            {
                FirstName = "Drexel";
                LastName = "Administrator";
                Address = "";
                Phone = "555.555.5555";
                Ext = "555";
                Email = "enroll@drexel.edu";
                Title = "Enrollment Management";
                UserName = "7777777";
            }
            else
            {
                try
                {
                    FirstName = ds.Tables["TerritoryManager"].Rows[0]["CounselorFirstName"].ToString();
                    LastName = ds.Tables["TerritoryManager"].Rows[0]["CounselorLastName"].ToString();
                    Address = ds.Tables["TerritoryManager"].Rows[0]["CounselorAddress"].ToString();
                    Phone = ds.Tables["TerritoryManager"].Rows[0]["CounselorPhone"].ToString();
                    Ext = ds.Tables["TerritoryManager"].Rows[0]["CounselorExtension"].ToString();
                    Email = ds.Tables["TerritoryManager"].Rows[0]["CounselorEmail"].ToString();
                    Title = ds.Tables["TerritoryManager"].Rows[0]["CounselorTitle"].ToString();
                    UserName = ds.Tables["TerritoryManager"].Rows[0]["CounselorUserName"].ToString();
                }
                catch
                {
                    FirstName = null;
                    LastName = null;
                    Address = null;
                    Phone = null;
                    Ext = null;
                    Email = null;
                    Title = null;
                    UserName = null;
                }
            }

			//get rid of dataset
			ds.Dispose();

		}

		#endregion
	}

