﻿
using System;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class _Default : System.Web.UI.Page 
{
    protected void Page_Load(object sender, EventArgs e)
    {
        requestAccessHandler handler = new requestAccessHandler();
        DataTable dtDrexelAdmin = handler.getDrexelContactInfo();
        lblContactName.Text = dtDrexelAdmin.Rows[0]["GC_DrexelAdminName"].ToString();
        lblContactEmail.Text = dtDrexelAdmin.Rows[0]["GC_DrexelAdminEmail"].ToString();
       // lblContactEmail.Attributes.Add("href","mailto:" + dtDrexelAdmin.Rows[0]["GC_DrexelAdminEmail"].ToString());
        lblContactNumber.Text = dtDrexelAdmin.Rows[0]["GC_DrexelAdminPhone"].ToString();
        

    }
}
