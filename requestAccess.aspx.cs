﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Text.RegularExpressions;

public partial class requestAccess : System.Web.UI.Page
{
    enum MessageCodes
    {
        Success = 0,
        ExistingEmail = 1,
        ExisitngAdmin = 2,
        NonExistingCeeb = 3,
        ExistingRequest = 4,
        NoAdmin = 5
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        cbLimited.Attributes.Add("OnClick", "jscript.validation.requestAccessCheckBox()");
        cbMisuse.Attributes.Add("OnClick", "jscript.validation.requestAccessCheckBox()");
        btnSubmit.Attributes.Add("OnClick", "jscript.popup.displayPopup()");
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        btnSubmit.Enabled = false;
        requestAccessHandler handler = new requestAccessHandler();
        String IP = Regex.Replace(Request.ServerVariables["REMOTE_HOST"], @"[0-9]+\.[0-9]+$", "*.*");
        int returnCode;

        //Form element display
        pnForm.Visible = false;
        pnResults.Visible = true;
        btnSubmit.Visible = false;

        returnCode = handler.requestCounselorAccess(2, IP, txtFirstName.Text, txtLastName.Text, txtCEEB.Text, txtEmail.Text, txtPhone.Text, ddlContact.SelectedValue);
        displayMessage(returnCode);
        //populate labels
        //lblHeaderDesc.Text = "Please review the information below";
        lblFirstName.Text = txtFirstName.Text;
        lblLastName.Text = txtLastName.Text;
        lblCEEB.Text = txtCEEB.Text;
        lblPhone.Text = txtPhone.Text;
        lblEmail.Text = txtEmail.Text;
        lblContact.Text = ddlContact.SelectedValue;
        panel1.Visible = false;

    }
    private void displayMessage(int messageCode)
    {
        switch (messageCode)
        {
            case (int)MessageCodes.Success:
                lblResult.CssClass = "OKMessage";
                lblResult.Text = "You have successfully submitted your request.";
                break;
            case (int)MessageCodes.ExisitngAdmin:
                lblResult.CssClass = "ErrorMessage";
                lblResult.Text = "An Adminstrator is already registered at this school! Please Request Counselor Access.";
                lbEdit.Visible = true;
                break;
            case (int)MessageCodes.ExistingEmail:
                lblResult.CssClass = "ErrorMessage";
                lblResult.Text = "This email is already registered! Please try another.";
                lbEdit.Visible = true;
                break;
            case (int)MessageCodes.NonExistingCeeb:
                lblResult.CssClass = "ErrorMessage";
                lblResult.Text = "This CEEB code you entered is not in our database! Please edit your submission.";
                lbEdit.Visible = true;
                break;
            case (int)MessageCodes.NoAdmin:
                lblResult.CssClass = "ErrorMessage";
                lblResult.Text = "This Ceeb is not registered.  Please click on the register link above.";
                lbEdit.Visible = true;
                break;
        }
    }
    protected void lbEdit_Click(object sender, EventArgs e)
    {
        pnForm.Visible = true;
        pnResults.Visible = false;
        lbEdit.Visible = false;
        btnSubmit.Visible = true;
    }
}
