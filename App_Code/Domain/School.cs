using System;
using System.Data;


	/// <summary>
	/// Summary description for School.
	/// </summary>
	public class School
	{
		#region Private Variables
		string _Name;
		string _Address;
		string _Address2;
		string _City;
		string _State;
		string _Zip;
		#endregion

		#region Properties
		public string Name
		{
			get{return _Name;}
			set{_Name = value;}
		}
		public string Address
		{
			get{return _Address;}
			set{_Address = value;}
		}
		public string Address2
		{
			get{return _Address2;}
			set{_Address2 = value;}
		}
		public string City
		{
			get{return _City;}
			set{_City = value;}
		}
		public string Zip
		{
			get{return _Zip;}
			set{_Zip = value;}
		}
		public string State
		{
			get{return _State;}
			set{_State = value;}
		}
#endregion

		#region Constructors
		public School(string ceeb)
		{
			getSchoolInfoByCeeb(ceeb);
		}
		#endregion

		#region Methods
		private void getSchoolInfoByCeeb(string ceeb)
		{
            if (ceeb == "7777777")
            {
                Name = "Your School";
                Address = "123 East West Street";
                Address2 = "Suite 123";
                City = "Philadephia";
                State = "PA";
                Zip = "19129";
            }
            else
            {
                try
                {
                    DataSet ds = new DataSet();
                    SQL data = new SQL();
                    ds.Tables.Add(data.getSchoolInfoByCeeb(ceeb));
                    Name = ds.Tables["SchoolInfo"].Rows[0]["highschoolname"].ToString();
                    Address = ds.Tables["SchoolInfo"].Rows[0]["highschooladdress"].ToString();
                    Address2 = ds.Tables["SchoolInfo"].Rows[0]["Address1"].ToString();
                    City = ds.Tables["SchoolInfo"].Rows[0]["city"].ToString();
                    State = ds.Tables["SchoolInfo"].Rows[0]["state"].ToString();
                    Zip = ds.Tables["SchoolInfo"].Rows[0]["Zip"].ToString();
                    ds.Dispose();
                }
                catch
                {
                    Name = null;
                    Address = null;
                    Address2 = null;
                    City = null;
                    State = null;
                    Zip = null;
                }
            }			
		}
		#endregion
	}

