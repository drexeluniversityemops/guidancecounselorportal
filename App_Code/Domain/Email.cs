using System;
using System.Web.Mail;
using System.Text;

namespace GC_Site
{
	/// <summary>
	/// Summary description for Email.
	/// </summary>
	public class Email
	{
		public Email()
		{
			//
			// TODO: Add constructor logic here
			//
		}
		public void sendNotification( string firstName, string lastName, string email, string userName, string userPassword, string counselorName, string counselorTitle, string counselorPhone, string counselorEmail)
		{
			StringBuilder mailBody = new StringBuilder();
			MailMessage mail = new MailMessage();
			/*string mailBody = "";
						mailBody += Convert.ToString(Session["FirstName"]);
						mailBody += "\r\n";*/
			mailBody.Append("Dear ");
			mailBody.Append(firstName + "  "  + lastName);
			mailBody.Append(",");
			mailBody.Append("\r\n");
			mailBody.Append("\r\n");
			mailBody.Append("Your request to access the Drexel Admissions Update for Guidance Counselors has been granted."); 
			mailBody.Append("\r\n");
			mailBody.Append("\r\n");
			mailBody.Append("Please be advised that use of and access to this website is limited to Drexel University and registered high school ");
					
			mailBody.Append("guidance and college advisors. Any misuse of this site including sharing of passwords, sharing of admission status reports ");
				
			mailBody.Append("with non-registered parties, sharing of admission or applicant information will be cause for immediate loss of access for  ");
			mailBody.Append("the high school. "); 
			mailBody.Append("\r\n");
			mailBody.Append("Username:  " + userName);
			mailBody.Append("\r\n");

				
			mailBody.Append("Password:  " + userPassword);
			mailBody.Append("\r\n");
			mailBody.Append("\r\n");
			mailBody.Append("Use this information to log in to http://deptdev.irt.drexel.edu/em/guidancecounselor/site/main.aspx. Note: You will be required to create a new password upon first login. ");
			mailBody.Append("\r\n");
			mailBody.Append("\r\n");
			mailBody.Append("If you have any questions about Drexel Admissions Update for Guidance Counselors, please contact me at em_ops@drexel.edu. ");
			mailBody.Append("\r\n");
			mailBody.Append("\r\n");
			mailBody.Append("Sincerely, ");
			mailBody.Append("\r\n");
			mailBody.Append("\r\n");
			mailBody.Append(counselorName);
			mailBody.Append("\r\n");
			mailBody.Append("Undergraduate Admissions ");
			mailBody.Append("\r\n");
			mailBody.Append(counselorTitle);
			mailBody.Append("\r\n");
			mailBody.Append(counselorPhone);
			mailBody.Append("\r\n");
			mailBody.Append(counselorEmail);
			mailBody.Append("\r\n");			

			mail.To = email;
			mail.From = "enroll@drexel.edu";
			mail.BodyFormat = MailFormat.Text;	
			mail.Subject = "Account Access to Drexel Admissions Update ";
			mail.Body = Convert.ToString(mailBody);
			SmtpMail.SmtpServer = "smtp.mail.drexel.edu";
			SmtpMail.Send(mail);
				
		}
	}
}
