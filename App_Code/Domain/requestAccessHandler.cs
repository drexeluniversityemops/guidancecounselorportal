﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

/// <summary>
/// Summary description for requestAccessHandler
/// </summary>
public class requestAccessHandler
{
    public requestAccessHandler()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    public User authenticateUser(String email, String password)
    {
        User user;
        user = SQL.authencticateUser(email, password);
        return user;
    }
    public void changePassword(int userID, String oldPassword, String newPassword)
    {
        SQL.changePassword(userID, oldPassword, newPassword);
    }
    public Boolean checkValidatedUserAgainstDB(User user)
    {
        User verifiedUser = authenticateUser(user.UserEmail, user.UserPassword);
        if (user.UserID == verifiedUser.UserID)
        {
            return true;
        }

        return false;
    }
    public void denyAccess(int id, String firstName, String lastName, String email)
    {
        SQL.denyAccessToUser(id);
        sendConfirmation(email, firstName, lastName, "denied. If you have any questions as to why, please contact the Admission Office at 1-800-2-DREXEL or at <a herf=\"mailto:enroll@drexel.edu\">enroll@drexel.edu</a>.", "");
    }
    public DataTable getCounselorRequest(String ceeb)
    {
        return SQL.getNewCounselorUsers(ceeb);
    }
    public DataTable getDrexelContactInfo()
    {
        return SQL.getDrexelContactInfo();
    }
    public DataTable getRequests(String email)
    {
        return SQL.getNewUsers(email);
    }
    public DataTable getUserRequestReport()
    {
        return SQL.getUserRequestReport();
    }
    public void grantAccess(int id, String firstName, String lastName, String email, String password)
    {
        SQL.grantAccessToUser(id, password);
        sendConfirmation(email, firstName, lastName, "granted", " Your username is your email address and your temporary password is: <b>" + password + "</b>.  You will be instructed to create your own unique password when you log in for the first time.  Should you have any problems gaining access, please contact the Admission Office at 1-800-2-DREXEL or at <a herf=\"mailto:enroll@drexel.edu\">enroll@drexel.edu</a>.");
    }
    public void grantAccessToCounselor(int id, String firstName, String lastName, String email, String password)
    {

        SQL.grantAccessToCounsleor(id, password);
        sendConfirmation(email, firstName, lastName, "granted", ". Your username is your email address and your password is: <b>" + password + "</b>.  Should you have any problems gaining access, please contact the Admission Office at 1-800-2-DREXEL or at <a herf=\"mailto:enroll@drexel.edu\">enroll@drexel.edu</a>.");
    }
    public int requestAccess(int typeOfAccess, String IP, String firstName, String lastName, int ceeb, String email, String phone, String bestTimeToContact)
    {
        int returnCode = SQL.insertRequestor(typeOfAccess, IP, firstName, lastName, email, phone, ceeb, bestTimeToContact);
        if (returnCode == 0)
        {
            sendEmail(typeOfAccess, IP, firstName, lastName, ceeb.ToString(), email, phone, bestTimeToContact);
            //sendNotification();
        }
        return returnCode;
    }
    public int requestCounselorAccess(int typeOfAccess, String IP, String firstName, String lastName, String ceeb, String email, String phone, String bestTimeToContact)
    {
        int returnCode = SQL.insertCounselor(typeOfAccess, IP, firstName, lastName, email, phone, ceeb, bestTimeToContact);
        if (returnCode == 0)
        {
            sendEmail(typeOfAccess, IP, firstName, lastName, ceeb, email, phone, bestTimeToContact);
        }
        return returnCode;
    }
    public void sendEmail(int typeOfAccess, String IP, String firstName, String lastName, String ceeb, String email, String phone, String bestTimeToContact)
    {
        Email emailMessage = new Email("admissions@drexel.edu", email, "");//ds 11/17/14
        StringBuilder emailBody = new StringBuilder();

        emailBody.Append("<html>");
        emailBody.Append("<head>");
        emailBody.Append("</head>");
        emailBody.Append("<body>");
        emailBody.Append("<table width=\"100%\">");
        emailBody.Append("<tr><td>");
 //       emailBody.Append("<img src=\"http://depttest1.drexel.edu/em/guidancecounselorportal/images/header_new.jpg\">");
        //ds 9/11/14
        emailBody.Append("<img src=\"" + System.Configuration.ConfigurationManager.AppSettings["header_new"] + "\">");

        emailBody.Append("</td></tr>");
        emailBody.Append("<tr><td>");
        emailBody.Append("<span style=\"font:Arial;font-weight:bold;font-size:14pt;\">Drexel University - Guidance Counselor Portal</span>");
        emailBody.Append("</td></tr>");
        emailBody.Append("<tr><td>Hello, ");
        emailBody.Append(firstName);
        emailBody.Append(".");
        emailBody.Append("<P>Thank you for requesting access to Drexel University’s Guidance Counselor Portal!  This new ");
        emailBody.Append("<br />tool provides you the opportunity to check the admission status of your students, as well as to keep up to ");
        emailBody.Append("<br />date regarding important news about Drexel University's admissions process.  ");
        emailBody.Append("<p />Please confirm that the information below is accurate:");
        emailBody.Append("</td></tr>");
        emailBody.Append("<tr><td>");
        emailBody.Append("<b>First Name:</b> ");
        emailBody.Append(firstName);
        emailBody.Append("</td></tr>");
        emailBody.Append("<tr><td>");
        emailBody.Append("<b>Last Name:</b> ");
        emailBody.Append(lastName);
        emailBody.Append("</td></tr>");
        emailBody.Append("<tr><td>");
        emailBody.Append("<b>Ceeb:</b> ");
        emailBody.Append(ceeb);
        emailBody.Append("</td></tr>");
        emailBody.Append("<tr><td>");
        emailBody.Append("<b>Telephone:</b> ");
        emailBody.Append(phone);
        emailBody.Append("</td></tr>");
        emailBody.Append("<tr><td>");
        emailBody.Append("<b>Email:</b> ");
        emailBody.Append(email);
        emailBody.Append("</td></tr>");
        emailBody.Append("<tr><td>");
        emailBody.Append("<b>Best Time to Contact:</b> ");
        emailBody.Append(bestTimeToContact);
        emailBody.Append("</td></tr>");
        emailBody.Append("<tr><td>");
        emailBody.Append("<p />A representative from Drexel University will be contacting you shortly to confirm your registration request.  Once registration is confirmed, you will be granted access to the Guidance Counselor Portal.  We look forward to working with you in the future!");
        emailBody.Append("<P>The Application Management Team");
        emailBody.Append("<BR>Division of Enrollment Management");
        emailBody.Append("<BR>Drexel University"); 
        emailBody.Append("</td></tr>");
        emailBody.Append("<tr><td>");
        emailBody.Append("Please do not respond to this email");
        emailBody.Append("</td></tr>");
        emailBody.Append("<table>");
        emailBody.Append("</body>");
        emailBody.Append("</html>");
        emailMessage.SendEmail(emailBody.ToString(), "Drexel University Guidance Counselor Portal Access Request");
    }
    public void sendNotification(String firstName, String lastName, int ceeb)
    {
        StringBuilder emailBody = new StringBuilder();
        String email = SQL.getCounselorEmailByCeeb(ceeb);
        Email emailMessage = new Email("admissions@drexel.edu", email, "bsf28@drexel.edu");//"bsf28@drexel.edu
        String schoolName = SQL.getSchoolNameByCeeb(ceeb);
        emailBody.Append("<html>");
        emailBody.Append("<head>");
        emailBody.Append("</head>");
        emailBody.Append("<body>");
        emailBody.Append("<table width=\"100%\">");
        emailBody.Append("<tr><td>");
 //       emailBody.Append("<img src=\"http://depttest1.drexel.edu/em/guidancecounselorportal/images/header_new.jpg\">");
        //ds 9/11/14
        emailBody.Append("<img src=\"" + System.Configuration.ConfigurationManager.AppSettings["header_new"] + "\">");

        emailBody.Append("</td></tr>");
        emailBody.Append("<tr><td>");
        emailBody.Append("There is a new request from ");
        emailBody.Append(firstName);
        emailBody.Append(" ");
        emailBody.Append(lastName);
        emailBody.Append(" for access to ");
        emailBody.Append(schoolName);
//        emailBody.Append(". Please go to the <a href=\"http://deptdev.irt.drexel.edu/em/guidancecounselorportal/tools/admin.aspx\">Admin Site</a> to verify.");
        //ds 9/11/14
        emailBody.Append(". Please go to the <a href=\"" + System.Configuration.ConfigurationManager.AppSettings["admin"] + "\">Admin Site</a> to verify.");

        emailBody.Append("</td></tr>");
        emailBody.Append("<tr><td>");
        emailBody.Append("Please do not respond to this email");
        emailBody.Append("</td></tr>");
        emailBody.Append("<table>");
        emailBody.Append("</body>");
        emailBody.Append("</html>");
        emailMessage.SendEmail(emailBody.ToString(), "New School Access");
    }
    public void sendConfirmation(String email, String firstName, String lastName, String message, String footer)
    {
        Email emailMessage = new Email("enroll@drexel.edu", email, "");
        StringBuilder emailBody = new StringBuilder();
        emailBody.Append("<html>");
        emailBody.Append("<head>");
        emailBody.Append("</head>");
        emailBody.Append("<body>");
        emailBody.Append("<table width=\"100%\">");
        emailBody.Append("<tr><td>");
//        emailBody.Append("<img src=\"http://depttest1.drexel.edu/em/guidancecounselorportal/images/header_new.jpg\">");
        //ds 9/11/14
        emailBody.Append("<img src=\"" + System.Configuration.ConfigurationManager.AppSettings["header_new"] + "\">");

        emailBody.Append("</td></tr>");
        emailBody.Append("<tr><td>");
        emailBody.Append("Hello ");
        emailBody.Append(firstName);
        emailBody.Append(".<br />Your request for access to the Drexel University Guidance Counselor Portal has been ");
        emailBody.Append(message);
//        emailBody.Append(". please <a href=\"http://depttest1.drexel.edu/em/guidancecounselorportal/login.aspx\"> click here </a>to log into the site.");
        //ds 9/1/14
        emailBody.Append("<. please <a href\"" + System.Configuration.ConfigurationManager.AppSettings["login"] + "\">click here</a>to log into the site.");

        emailBody.Append(footer);
        emailBody.Append("</td></tr>");
        emailBody.Append("<tr><td>");
        emailBody.Append("<p />Please don't respond to this email");
        emailBody.Append("</td></tr>");
        emailBody.Append("<table>");
        emailBody.Append("</body>");
        emailBody.Append("</html>");
        emailMessage.SendEmail(emailBody.ToString(), "Update: Drexel University Guidance Counselor Portal Access Request");
    }
    public void sendCounselorConfirmation(String adminEmail, String email, String firstName, String lastName, String message, String footer)
    {
        StringBuilder emailBody = new StringBuilder();
        Email emailMessage = new Email("admissions@drexel.edu", email, "");
        emailBody.Append("<html>");
        emailBody.Append("<head>");
        emailBody.Append("</head>");
        emailBody.Append("<body>");
        emailBody.Append("<table width=\"100%\">");
        emailBody.Append("<tr><td>");
 //       emailBody.Append("<img src=\"http://depttest1.drexel.edu/em/guidancecounselorportal/images/header_new.jpg\">");
        //ds 9/11/14
        emailBody.Append("<img src=\"" + System.Configuration.ConfigurationManager.AppSettings["header_new"] + "\">");

        emailBody.Append("</td></tr>");
        emailBody.Append("<tr><td>");
        emailBody.Append("There is a new request from ");
        emailBody.Append(firstName);
        emailBody.Append(" ");
        emailBody.Append(lastName);
        emailBody.Append(" for access to the Drexel Guidance Counselor Portal");
//        emailBody.Append(". Please go to the <a href=\"http://deptdev.irt.drexel.edu/em/guidancecounselorportal/login.aspx\">Guidance Counselor Portal</a> to verify.");
        //ds 9 /11/14
        emailBody.Append(". Please go to the <a href=\"" + System.Configuration.ConfigurationManager.AppSettings["login"] + "\">Guidance Counselor Portal</a> to verify.");

        emailBody.Append("</td></tr>");
        emailBody.Append("<tr><td>");
        emailBody.Append("Please do not respond to this email");
        emailBody.Append("</td></tr>");
        emailBody.Append("<table>");
        emailBody.Append("</body>");
        emailBody.Append("</html>");
        emailMessage.SendEmail(emailBody.ToString(), "New School Access");
    }

}
