﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="main.aspx.cs" Inherits="main" %>

<%@ Register src="Controls/events.ascx" tagname="events" tagprefix="uc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Drexel GC Portal</title>
    <link href="styles/GuidanceConselor.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
        <div id="Header" runat="server">
            <table style="width:100%;background-color:#666699;" >
                <tr>
                    <td>
                        <img src="images/header_new.jpg" alt="logo" />
                    </td>
                </tr>
            </table>
        </div>
        <div id="NavBar">
            <table style="width:100%;">
                <tr>
                    <td width="90%" style="padding:20px;">
                        <asp:Label ID="lblHello" runat="server" Text="" CssClass="uportal-channel-text"></asp:Label>
                        &nbsp;<asp:LinkButton ID="lbAdminTools" runat="server" onclick="lbAdminTools_Click" 
                            Visible="False" CssClass="uportal-channel-text">Admin Tools</asp:LinkButton>
                    </td>
                    <td>
                        <div align="center">
                            <a href="default.aspx" ><img height="26" alt="help"  src="images/help_dft.gif" width="39" border="0" name="help" />
                            <br />
							<span class="uportal-label">help</span>
							</a>
					    </div>
                    </td>
                </tr>
            </table>
        </div>
        <div id="Body" runat="server" style="padding-left:20px;">
        <table width="100%">
            <tr>
                <td style="width:30%;" valign="top">
                        <table style="width:85%">
                        <tr>
				            <td class="SectionHeader">&nbsp;</td>
				        </tr>
<%--				        <tr>
				            <td class="uportal-head14-bold">Your Drexel contact is:</td>
				        </tr>--%>
				        <tr>
				            <td>
				                <asp:Label runat="server" Text="" ID="lblTerritoryManager" CssClass="uportal-channel-text"></asp:Label>
				            </td>
				        </tr>
				        <tr>
				            <td><IMG height="20" alt="" src="images/dot_clear.gif" width="1"></td>
				        </tr>
				        <tr>
				            <td class="SectionHeader">&nbsp;</td>
				        </tr>
				        <tr>
				            <td class="uportal-head14-bold">Your High School is:</td>
				        </tr>
				        <tr>
				            <td>
				            <asp:Label runat="server" Text="" ID="lblSchool" CssClass="uportal-channel-text"></asp:Label>
				            </td>
				        </tr>
				        <tr>
				            <td><IMG height="20" alt="" src="images/dot_clear.gif" width="1"></td>
				        </tr>
				        <tr>
				            <td class="SectionHeader">&nbsp;</td>
				        </tr>
				        <tr>
				            <td class="uportal-head14-bold">Your High School Administrator is:</td>
				        </tr>
				        <tr>
				            <td>
				                <asp:Label runat="server" Text="" ID="lblSchoolAdminInfo" CssClass="uportal-channel-text"></asp:Label>
				            </td>
				        </tr>
                        </table>
                </td>
                <td style="width:33%;" valign="top">
                    <asp:Panel ID="pnNotifications" runat="server" Visible="true">
                        <table style="width:85%">
                            <tr>
                                <td class="SectionHeader">
                                    &nbsp;</td>
                            </tr>
				            <tr>
				                <td class="uportal-head14-bold">Drexel Updates for your High School</td>
				            </tr>
				            <tr>
				                <td>
                                    <asp:Label ID="lblUpdates" runat="server" Text="" CssClass="uportal-channel-text"></asp:Label>
                                </td>
				            </tr>
                        </table>
                    </asp:Panel>
                    <table style="width:85%">
                            <tr>
				                <td class="SectionHeader">&nbsp;</td>
				            </tr>
				            <tr>
				                <td class="uportal-head14-bold">View Applicant Status</td>
				            </tr>
				            <tr>
				                <td>
				                    <ul>
                                        <li>
										    <asp:linkbutton id="AllStudents" Runat="server" CommandName="All" 
                                                onclick="AllStudents_Click1" CssClass="uportal-channel-text">All Applications</asp:linkbutton>
										<li>
									        <asp:linkbutton id="InCompleteStudents" Runat="server" CommandName="Incomplete" 
                                                onclick="InCompleteStudents_Click1" CssClass="uportal-channel-text">Incomplete Students</asp:linkbutton>
										<li>
									        <asp:linkbutton id="CompleteStudents" Runat="server" CommandName="Complete" 
                                                onclick="CompleteStudents_Click1" CssClass="uportal-channel-text">Complete Students</asp:linkbutton>
										</li>
							        </ul>
				                </td>
				            </tr>
				            <tr>
				                <td class="SectionHeader">&nbsp;</td>
				            </tr>
				            <tr>
				                <td class="uportal-head14-bold">Quick Links</td>
				            </tr>
				            <tr>
				                <td class="uportal-channel-text">

										<a href="http://www.drexel.edu/em/undergrad/about/" target="_blank">About Drexel</a><br>
										<a href="http://www.drexel.edu/em/undergrad/academics/" target="_blank">Academic 
										Programs</a><br>
										<a href="http://www.drexel.edu/em/undergrad/coop/" target="_blank">Drexel Co-op</a><br>
										<a href="http://www.drexel.edu/em/undergrad/financialaid/" target="_blank">Financing 
										Options</a><br>
										<a href="http://www.drexel.edu/em/undergrad/studentlife/" target="_blank">Student 
										Life</a><br>
										<a href="http://www.drexel.edu/admissions/visiting.aspx" target="_blank">Visiting 
										Drexel</a><br>
										<a href="http://www.drexel.edu/em/apply/undergrad/default.html" target="_blank">How 
										to Apply</a><br>
										<a href="http://www.drexel.edu/em/contact/" target="_blank">Contact Admissions</a>
										
				                </td>
				            </tr>
                    </table>
                </td>
                <td style="width:36%;">
                    <table style="width:85%">
                        <tr>
				            <td class="SectionHeader">&nbsp;</td>
				        </tr>
				        <tr>
				            <td class="uportal-head14-bold">Events</td>
				        </tr>
				        <tr>
				            <td><uc1:events ID="events1" runat="server" /></td>
				        </tr>
                        </table>
                    
                    
                </td>
            </tr>
        </table>
      </div>
    </form>
</body>
</html>
