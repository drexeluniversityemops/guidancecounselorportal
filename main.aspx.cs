﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class main : System.Web.UI.Page
{
    Counselor terrManager;
    School school;
    User user;
    Administrator schoolAdmin;
    AnnouncementList announcements;
    int NumCompStudents = 0;
    protected System.Web.UI.WebControls.ImageButton ImageButton1;
    int NumInCompStudents = 0;
    protected void Page_Load(object sender, EventArgs e)
    {
        user = (User)Session["CurrUser"];
        if (Session["Auth"] == null)
        {
            Response.Redirect("login.aspx");
        }
        if (user.TypeOfAccess == "Administrator")
        {
            lbAdminTools.Visible = true;
        }
        else
        {
            lbAdminTools.Visible = false;
        }
        initializePageData();
        initilizeDisplay();
        //AdminTools.Visible = false;
    }
    private void initilizeDisplay()
    {
        #region Set Page Labels
        lblHello.Text = String.Format("Welcome {0}!</font>", user.UserFirstName);
        //currentlyLoggedAs.InnerText = String.Format("You are logged on as a {0} to Drexel Admissions update.", user.UserType);
//        lblTerritoryManager.Text = String.Format("{0} {1}<BR>{2}<br>{3}<br>{4}<br><a href=\"mailto:{5}\">{5}</a>", terrManager.FirstName, terrManager.LastName, terrManager.Title, terrManager.Address, terrManager.Phone, terrManager.Email); //ds 9/29/14 - commented out as requested
        lblTerritoryManager.Text = "If you have questions related to your applicants please contact <a href=\"mailto:enroll@drexel.edu\">enroll@drexel.edu</a> or 1-800-2-DREXEL";//ds 9/29/14
        lblSchool.Text = String.Format("{0}<br>{1} {2}<br>{3}, {4} {5}", school.Name, school.Address, school.Address2, school.City, school.State, school.Zip);
        lblSchoolAdminInfo.Text = String.Format("{0} {1}<br><a href=\"mailto:{2}\">{2}</a>", schoolAdmin.FirstName, schoolAdmin.LastName, schoolAdmin.Email);
        #endregion

        #region Set Announcements
        List<String> announcements1 = SQL.getUniversalAnnouncements();
        foreach (String announcement in announcements1)
        {
            lblUpdates.Text += announcement;
        }
        #endregion

        #region Set Student Links
        //set the student link count
        if (NumCompStudents == 1)
        {
            CompleteStudents.Text = NumCompStudents + " Complete Application";
        }
        else
        {
            CompleteStudents.Text = NumCompStudents + " Complete Applications";
        }
        if (NumInCompStudents == 1)
        {
            InCompleteStudents.Text = NumInCompStudents + " Incomplete Application";
        }
        else
        {
            InCompleteStudents.Text = NumInCompStudents + " Incomplete Applications";
        }
        #endregion

        //#region Set Administrator Display
        ////set up the user display by type
        //if (user.UserType == "Administrator")
        //{
        //    AdminTools.Visible = true;
        //    AdminHideShow.Visible = false;
        //}
        //else
        //{
        //    AdminTools.Visible = false;
        //    AdminHideShow.Visible = true;
        //}
        //#endregion
    }
    private void initializePageData()
		{
			#region Instantiate Objects
            user = (User)Session["CurrUser"];
            school = new School(user.Ceeb);
            terrManager = new Counselor(user.Ceeb);
            schoolAdmin = new Administrator(user.Ceeb);
            announcements = new AnnouncementList(user.Ceeb);
            if (user.Ceeb == "7777777")
            {
                NumCompStudents = 4;
                NumInCompStudents = 2;
            }
            else
            {
                NumCompStudents = SQL.getStudentCount(user.Ceeb, 'C');
                NumInCompStudents = SQL.getStudentCount(user.Ceeb, 'I');
            }
			#endregion
		}

    protected void AllStudents_Click1(object sender, EventArgs e)
    {
        Response.Redirect(String.Format("AllStudents.aspx?id={0}", user.Ceeb));
    }
    protected void InCompleteStudents_Click1(object sender, EventArgs e)
    {
        Response.Redirect(String.Format("IncompleteStudents.aspx?id={0}", user.Ceeb));
    }
    protected void CompleteStudents_Click1(object sender, EventArgs e)
    {
        Response.Redirect(String.Format("CompleteStudents.aspx?id={0}", user.Ceeb));
    }
    protected void lbAdminTools_Click(object sender, EventArgs e)
    {
        Response.Redirect("CounselorAdmin.aspx");
    }
}
