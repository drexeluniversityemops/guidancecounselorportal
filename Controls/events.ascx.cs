﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class Controls_events : System.Web.UI.UserControl
{

    protected System.Web.UI.WebControls.DataGrid Datagrid1;
    protected System.Web.UI.WebControls.DataGrid Datagrid2;
    protected DataSet myDataSet;
    protected DataView myDataView;
    protected DataView myDataView2;
    protected DataRowView row;
    protected void Page_Load(object sender, EventArgs e)
    {
        //instantiate objects and fill data
        SQL data = new SQL();
        myDataSet = new DataSet();
        myDataSet.Tables.Add(data.getEvents());
        myDataSet.Tables.Add(data.getEventType());
        myDataSet.Tables.Add(data.getNoEvents());
        rptCategories.DataSource = myDataSet;
        rptCategories.DataMember = "EventType";
        rptCategories.DataBind();
        myDataView = myDataSet.Tables["Events"].DefaultView;
        myDataView2 = myDataSet.Tables["NoEvent"].DefaultView;
        myDataView.Sort = "EventLevelID";
    }
    #region Public Methods
    public DataView CheckForEvents(int intEventTypeID)
    {
        myDataView = myDataSet.Tables["Events"].DefaultView;
        myDataView2 = myDataSet.Tables["NoEvent"].DefaultView;
        myDataView.RowFilter = "EventTypeID=" + intEventTypeID;

        if (myDataView.Count == 0)
        {
            myDataView2.RowFilter = "EventTypeID=" + intEventTypeID;
            return myDataView2;
        }
        else
        {
            return null;
        }

    }

    public DataView FilterEvents(int intEventTypeID)
    {
        myDataView = myDataSet.Tables["Events"].DefaultView;
        myDataView2 = myDataSet.Tables["NoEvent"].DefaultView;
        myDataView.RowFilter = "EventTypeID=" + intEventTypeID;
        return myDataView;
    }
    #endregion
}
